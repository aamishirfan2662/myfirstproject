<?php

Route::get('/dashboard', 'DashboardController@index')->name('admin.index');



Route::resource('subjects', 'SubjectController');

Route::get('files/create', 'FileUploadController@create')->name('files.create');
Route::post('files', 'FileUploadController@store')->name('files.store');
Route::get('files/{subject}/{file}', 'FileUploadController@show')->name('files.show');
Route::get('files', 'FileUploadController@index')->name('files.index');
Route::patch('files/{file}', 'FileUploadController@update')->name('files.update');
Route::delete('files/{file}', 'FileUploadController@destroy')->name('files.destroy');
//Route::resource('fileable.files', 'FileUploadController')->except(['fileable.file.index', 'fileable.file.create', 'fileable.file.store']);

Route::get('downloadables/create', 'DownloadableController@create')->name('downloadables.create');
Route::post('downloadables', 'DownloadableController@store')->name('downloadables.store');
Route::get('downloadables/{category}/{downloadable}', 'DownloadableController@show')->name('downloadables.show');
Route::get('downloadables', 'DownloadableController@index')->name('downloadables.index');
Route::patch('downloadables/{downloadable}', 'DownloadableController@update')->name('downloadables.update');
Route::delete('downloadables/{downloadable}', 'DownloadableController@destroy')->name('downloadables.destroy');
//Route::resource('downloadables', 'DownloadableController');

Route::post('jobs', 'JobController@store')->name('jobs.store');
Route::get('jobs', 'JobController@index')->name('jobs.index');
Route::get('jobs/{subCategory}/{job}', 'JobController@show')->name('jobs.show');
Route::patch('jobs/{job}', 'JobController@update')->name('jobs.update');
Route::delete('jobs/{job}', 'JobController@destroy')->name('jobs.destroy');
Route::get('jobs/create','JobController@create')->name('jobs.create');
//Route::resource('jobs', 'JobController');

//Route::resource('tests', 'TestController');
//Route::resource('jobs-tests', 'JobController');

Route::resource('tags', 'TagController');

Route::resource('blogs', 'BlogController');

Route::resource('useruploads', 'UserUploadController');

Route::get('useruploads/{userupload}', 'DownloadController@download')->name('downloads.download');

Route::get('user', 'DownloadController@sample')->name('downloads.sample');

Route::get('user-downloadables', 'DownloadableController@index')->name('user-downloadables.index');


Route::get('subject-quizzes/create', 'SubjectQuizController@create')->name('subject-quizzes.create');
Route::post('subject-quizzes', 'SubjectQuizController@store')->name('subject-quizzes.store');
Route::get('subject-quizzes/{subject}/{subject_quiz}', 'SubjectQuizController@show')->name('subject-quizzes.show');
Route::get('subject-quizzes', 'SubjectQuizController@index')->name('subject-quizzes.index');
Route::patch('subject-quizzes/{subject_quiz}', 'SubjectQuizController@update')->name('subject-quizzes.update');
Route::delete('subject-quizzes/{subject_quiz}', 'SubjectQuizController@destroy')->name('subject-quizzes.destroy');
//Route::resource('subject-quizzes', 'SubjectQuizController');

Route::get('job-quizzes/create', 'JobQuizController@create')->name('job-quizzes.create');
Route::post('job-quizzes', 'JobQuizController@store')->name('job-quizzes.store');
Route::get('job-quizzes/{job}/{job_quiz}', 'JobQuizController@show')->name('job-quizzes.show');
Route::get('job-quizzes', 'JobQuizController@index')->name('job-quizzes.index');
Route::patch('job-quizzes/{job_quiz}', 'JobQuizController@update')->name('job-quizzes.update');
Route::delete('job-quizzes/{job_quiz}', 'JobQuizController@destroy')->name('job-quizzes.destroy');
//Route::resource('job-quizzes', 'JobQuizController');

Route::get('test-quizzes/create', 'TestQuizController@create')->name('test-quizzes.create');
Route::post('test-quizzes', 'TestQuizController@store')->name('test-quizzes.store');
Route::get('test-quizzes/{category}/{test_quiz}', 'TestQuizController@show')->name('test-quizzes.show');
Route::get('test-quizzes', 'TestQuizController@index')->name('test-quizzes.index');
Route::patch('test-quizzes/{test_quiz}', 'TestQuizController@update')->name('test-quizzes.update');
Route::delete('test-quizzes/{test_quiz}', 'TestQuizController@destroy')->name('test-quizzes.destroy');
//Route::resource('test-quizzes', 'TestQuizController');

//Route::resource('course-quizzes', 'CourseQuizController');

//Route::post('course/{quiz}/{file_id}', 'CourseQuizController@deleteFile')->name('course.destroyFile');
Route::get('job-quizz/{job_quiz}/{file_id}', 'JobQuizController@deleteFile')->name('job-quiz.destroyFile');
Route::get('test-quizz/{test_quiz}/{file_id}', 'TestQuizController@deleteFile')->name('test-quiz.destroyFile');
Route::get('quiz/{quiz}/{file_id}', 'SubjectQuizController@deleteFile')->name('quiz.destroyFile');


Route::get('slider', 'SliderController@index')->name('slider');
//
Route::post('slider/{slider}/update', 'SliderController@update')->name('slider.update');

Route::get('about', 'AboutController@index')->name('admin.about');
//
Route::post('about/{about}/update', 'AboutController@update')->name('about.update');

Route::get('contact', 'ContactController@index')->name('admin.contact');
//
Route::post('contact/{contact}/update', 'ContactController@update')->name('contact.update');

Route::get('banner', 'BannerController@index')->name('banner');
//
Route::post('banner/{banner}/update', 'BannerController@update')->name('banner.update');

//Route::resource('slider', 'SliderController');
