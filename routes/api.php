<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/files/{subject}', 'Api\FileController@getSubjectFiles');

Route::get('/jobs/{category}', 'Api\JobController@getJobs');




/// MOBILE APP API's

/// auth api routes
Route::post('/register', 'Api\AuthController@register')->name('api.register');
Route::post('/login', 'Api\AuthController@login')->name('api.login');
Route::post('/forgot-password', 'Api\AuthController@forgot_password')->name('api.forgot_password');

// home api..... all data required on home is here
// 1- Categories with their subCategories or with quizzes (Admission\Entry Test category directly have quizzes against it)
// 2- Subjects with there quizzes
// 3- Quizzes against Admission\Entry Test can be fetch from here
// 4- Donwloadables (Date Sheets, Past Papers, Scholarships, Ads) with there downloadable files

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/home', 'Api\HomeController@index')->name('api.home');
    Route::get('/subjects', 'Api\SubjectController@index')->name('api.subjects');
    Route::get('/sub-categories', 'Api\SubCategoryController@index')->name('api.sub_categories');

    // get jobs or quizzes based on which given category has which relation.. i.e quizzes or jobs
    Route::get('/sub-category/{id}/jobs-or-quizzes', 'Api\JobOrQuizController@getJobsOrQuizzesUponSubcategory');

    // get quizzes against subjects and jobs
    Route::get('subjects/{id}/quizzes', 'Api\SubjectController@quizzes');
    Route::get('jobs/{id}/quizzes', 'Api\JobQuizController@index');


    Route::get('/quiz/{id}/detail', 'Api\QuizDetailController@show');

    Route::get('/quiz/{id}', 'Api\QuestionController@index');
    // save record api
    // save concise as well as detailecd record on submission of quiz
    Route::post('/quiz/record', 'Api\RecordController@store');
    // download file
    Route::get('download/{id}', 'Api\DownloadController@download');
    // report a bug
    Route::post('/quiz/report', 'Api\BugController@report');

    Route::get('/me', 'Api\AuthController@me')->name('api.me');

    Route::get('/user/record','Api\AuthController@userRecord')->name('api.me');
});
