<?php

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/', function (){
//    return view('home');
//})->name('home');

Route::resource('profile', 'ProfileController');

Route::get('/about', 'AboutController@index')->name('about');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::get('/useruploads', 'UserUploadController@create')->name('useruploads.create');
Route::post('/useruploads', 'UserUploadController@store')->name('useruploads.store');

Route::get('/jobs/{subCategory}', 'JobController@jobs')->name('jobs.jobs');

Route::get('/scholarships', 'ScholarshipController@index')->name('client.scholarships');
//Route::get('/scholarships/{blog}', 'ScholarshipController@show')->name('client.scholarships.show');
//Route::get('/past-papers', 'PastpaperController@index')->name('client.past-papers');
//Route::get('/past-papers/{blog}', 'PastpaperController@show')->name('client.past-papers.show');
Route::get('/job-ads', 'AdsController@index')->name('client.job-ads');
//Route::get('/job-ads/{blog}', 'AdsController@show')->name('client.job-ads.show');

Route::get('/blogs', 'BlogController@index')->name('client.blogs');
Route::get('/news', 'NewsController@index')->name('client.news');
Route::get('/blogs/{blog}', 'BlogController@show')->name('client.blogs.show');
Route::get('/news/{news}', 'NewsController@show')->name('client.news.show');
Route::get('blogs/tags/{tag}', 'TagController@showBlog')->name('tagged.showBlog');
Route::get('news/tags/{tag}', 'TagController@showNews')->name('tagged.showNews');
Route::get('/downloadables/{downloadable}', 'DownloadController@show')->name('client.downloadables.show');
Route::get('downloadable/{downloadable}', 'DownloadController@download')->name('client.downloadable.download');
Route::get('/sample', 'DownloadController@sample')->name('client.downloadable.sample');
Route::post('/comments/{blog}', 'CommentController@storeComment')->name('comments.store');
Route::post('/replies/{blog}', 'CommentController@storeReply')->name('replies.store');

// subjects
Route::get('subjects/{subject}/quizzes', 'SubjectQuizController@show')->name('subject.quizzes.show');
Route::get('subjects/{subject}/quizzes/{quiz}', 'SubjectQuizController@detail')->name('subject.quizzes.detail');

// jobs
Route::get('/jobs/{subCategory}/{job}/quizzes', 'JobQuizController@show')->name('job.quizzes.show');
Route::get('/jobs/{subCategory}/{job}/quizzes/{quiz}', 'JobQuizController@detail')->name('job.quizzes.detail');

// tests
Route::get('/tests/{subCategory}', 'TestQuizController@quizzes')->name('test.quizzes.show');
Route::get('/tests/{subCategory}/quizzes/{quiz}', 'TestQuizController@detail')->name('test.quizzes.detail');


Route::get('/start/quiz/{quiz}', 'AttemptController@index')->name('attempt.index');
//Route::get('/{subject}/courses/{quiz}/start', 'AttempController@index')->name('attempt.index');

Route::post('/email', 'ContactController@email')->name('contact.email');