<?php

namespace App;

use App\Helpers\GeneralTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Subject extends Model
{
    use GeneralTrait;

    protected $fillable = [
        'name',
        'image'
    ];


    /**
     * @return mixed|string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function quizzes()
    {
        return $this->morphMany(Quiz::class, 'quizable')->latest('updated_at');
    }
    /**
     * @param array $select
     * @return mixed
     */
    public function getSubjects()
    {
        return $this->with(['files', 'quizzes'])->latest()->paginate(10);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createSubject($request)
    {
        DB::transaction(function () use ($request) {
            $subject = $this->create($request->all());
//            dd($subject);
            $this->storeImage($subject, $request);
        });
    }


    /**
     * @param Subject $subject
     */
    public function getSubjectById(Subject $subject)
    {
        dd($subject);
    }

    /**
     * @param $request
     * @param Subject $subject
     * @return void
     */
    public function updateSubject($request, $subject)
    {
        DB::transaction(function () use ($request, $subject) {
//            dd($request);
            $subject->update($request->all());
            if (request()->has('image'))
            {
                $this->storeImage($subject, $request);
            }
        });
    }

    /**
     * @param Subject $subject
     * @return bool|null
     * @throws \Exception
     */
    public function destroySubject(Subject $subject)
    {
        if (file_exists(storage_path('app/public/'.$subject->getOriginal('image'))))
        {
            unlink(storage_path('app/public/'.$subject->getOriginal('image')));
        }
        return $subject->delete();
    }


}
