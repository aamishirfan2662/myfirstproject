<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class Downloadable extends Model
{

    public function getRouteKeyName()
    {
        return 'category';
    }
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function uploadFile($request)
    {
        $downloadable = Downloadable::where('id', $request->fileable_id)->first();
        $file = new File();
        $file->name = $request->name;
        $file->file = $request->file->store('user_downloads', 'public');

        DB::transaction(function () use ($request, $downloadable, $file){
            $downloadable->files()->save($file);
        });

    }

    public function getFiles()
    {
        return File::where('fileable_type', 'downloadables')->latest()->get();
    }

    public function updateFile($request, $downloadable)
    {
        $data = $request->all();
        if (request()->hasFile('file'))
        {
            $data = array_merge($request->all(), ['file' => $request->file->store('user_downloads', 'public')]);
        }
        DB::transaction(function () use($data, $downloadable){
            $downloadable->update($data);
        });
    }
}
