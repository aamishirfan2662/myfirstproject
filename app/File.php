<?php

namespace App;

use App\Imports\FileImport;
use function foo\func;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class File extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'file',
        'fileable_id'
    ];
    protected $dates = ['deleted_at'];


    /**
     * @return mixed|string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class, 'sub_category_id');
    }

    public function quizzes()
    {
        return $this->belongsToMany(Quiz::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function fileable()
    {
        return $this->morphTo();
    }

    /**
     * @return File[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getFiles()
    {
        return $this::with('fileable')->get();
    }

    public function getFilesWithQuizzes()
    {
        return $this::with('quizzes')->latest()->get();
    }

    /**
     * @param $request
     * @return mixed
     */
    public function uploadFile($request)
    {

        $this->name = $request->name;
        $this->file = $request->file->store('quiz_files', 'public');

        DB::transaction(function () use($request){
            $subject = Subject::find($request->fileable_id);
            $subject->files()->save($this);
            Excel::import(new FileImport($this->id), request()->file('file'));
        });
    }

    /**
     * @param $request
     * @param $file
     * @return mixed
     */
    public function updateFile($request, $file)
    {

        $data = $request->all();
        if (request()->hasFile('file'))
        {
            $data += ['file', $request->file->store('quiz_files', 'public')];

            DB::transaction(function () use ($file){
                Question::where('file_id', $file->id)->delete();
                Excel::import(new FileImport($file->id), request()->file('file'));
            });
        }

        $file->update($data);

    }

    /**
     * @param File $file
     * @return bool|null
     * @throws \Exception
     */
    public function destroyFile($file)
    {
        DB::transaction(function () use ($file){
            $file->quizzes()->detach();
            return $file->delete();
        });
    }

}
