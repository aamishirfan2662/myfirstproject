<?php

namespace App;

use App\Helpers\GeneralTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class Blog extends Model
{
    use GeneralTrait;
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'body',
        'type',
        'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

    public function getBlogs()
    {
        return $this->latest()->paginate(10);
    }

    public function scopeType($query, $type)
    {
        return  $query->where('type', $type);

    }

    public function createBlog($request)
    {
        DB::transaction(function () use ($request) {
            $blog = $this->create($request->all());
            $blog->update([
                'slug' => $blog->id.'-'.Str::slug($request->title, '-'),
            ]);

            $blog->tags()->attach($request->tags);
            $this->storeImage($blog, $request);
        });
    }

    public function updateBlog($blog, $request)
    {
        $data = $request->all();

        $data += ['slug' => $blog->id.'-'.Str::slug($request->title, '-')];

        DB::transaction(function () use ($data, $blog, $request){
            $blog->update($data);
            $blog->tags()->sync($request->tags);

            if (request()->has('image'))
            {
                $this->storeImage($blog, $request);
            }
        });

        return $blog;
    }

    public function deleteBlog($blog)
    {
        $blog->delete();
    }
}
