<?php

namespace App\Helpers;

use Intervention\Image\Facades\Image;

trait GeneralTrait
{
    public function storeImage($obj, $request)
    {
        $obj->update([
            'image' => $request->image->store('uploads', 'public'),
        ]);
//        $image = Image::make($obj->image)->resize(250,250);
//        $image->save();
    }
}