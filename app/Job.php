<?php

namespace App;

use App\Helpers\GeneralTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class Job extends Model
{
    use GeneralTrait;

    protected $fillable = [
        'name',
        'sub_category_id',
        'image'
    ];

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }


    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class, 'sub_category_id');
    }


    public function getRouteKeyName()
    {
        return 'name';
    }

    public function quizzes()
    {
        return $this->morphMany(Quiz::class, 'quizable')->latest('updated_at');
    }

    /**
     * @return JobTest[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getJobs()
    {
        return $this::with('subCategory')->latest()->paginate(10);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createJob($request)
    {
//        dd($request);
        DB::transaction(function () use ($request) {
            $job = $this->create($request->all());
            $this->storeImage($job, $request);
        });
    }

    /**
     * @param $job
     * @param $data
     * @param $request
     */
    public function updateJob($request, $job)
    {
        DB::transaction(function () use ($request, $job) {
            $job->update($request->all());
            if (request()->hasFile('image'))
            {
                $this->storeImage($job, $request);
            }
        });
    }

    public function deleteJob($job)
    {

        return $job->delete();
    }
}
