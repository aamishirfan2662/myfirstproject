<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    public function getRouteKeyName()
    {
        return 'name';
    }

    // accessor

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

    public function conciseRecord()
    {
        return $this->hasMany(ConciseRecord::class,'user_id');
    }

    public function detailedRecord()
    {
        return $this->hasMany(DetailedRecord::class,'user_id');
    }

    public function updateUser($user)
    {
        $data = request()->validate([
            'name' => 'required|string|max:100',
            'phone' => 'nullable|numeric',
            'dob' => 'sometimes',
            'gender' => 'required',
            'qualification' => 'sometimes',
            'present_status' => 'sometimes',
            'address' => 'sometimes',
            'image' => 'sometimes',
        ]);
        DB::transaction(function () use ($data, $user){
            $user->update($data);
            if (request()->has('image'))
            {
                $user->update([
                    'image' => request()->image->store('user', 'public'),
                ]);
            }
        });
        return $user;

    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'phone', 'dob', 'gender', 'qualification', 'present_status', 'address', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
