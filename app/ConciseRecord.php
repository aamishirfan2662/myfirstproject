<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConciseRecord extends Model
{
    protected $fillable = [
        'total_questions',
        'correct_answers',
        'user_id',
    ];
}
