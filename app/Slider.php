<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['image','primary_text', 'secondary_text'];

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

}
