<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function answers(){

        return $this->hasMany(Answer::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }
}
