<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailedRecord extends Model
{
    protected $fillable = [
        'quiz_name',
        'question',
        'correct_answer',
        'answered',
        'user_id'
    ];

    public function user()
    {
        return $this->$this->belongsTo(User::class);
    }
}
