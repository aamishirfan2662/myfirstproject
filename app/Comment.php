<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function createComment($blog)
    {
//        dd($blog);
        $data = request()->validate([
            'body' => 'required',
        ]);

        $this->user_id = Auth::user()->id;
        $this->parent_id = null;
        $this->body = $data['body'];

        DB::transaction(function () use ($blog){
            $blog->comments()->save($this);
        });

    }

    public function createReply($blog)
    {
//        dd($blog);
        $data = request()->validate([
            'reply' => 'required',
        ]);

        $this->user_id = Auth::user()->id;
        $this->parent_id = request()->input('comment_id');
        $this->body = $data['reply'];
        DB::transaction(function () use ($blog){
            $blog->comments()->save($this);
        });

    }
}
