<?php

namespace App;

use App\Helpers\GeneralTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Test extends Model
{
    use GeneralTrait;

    protected $guarded = [];


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

    public function quizzes()
    {
        return $this->morphMany(Quiz::class, 'quizable');
    }

    /**
     * @return JobTest[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getTests()
    {
        return $this->latest()->get();
    }

    /**
     * @param $data
     * @param $request
     * @return mixed
     */
    public function createTest($data, $request)
    {
        DB::transaction(function () use ($data, $request){
            $test = $this->create($data);
            $this->storeImage($test, $request);
        });
    }

    /**
     * @param $test
     * @param $data
     * @param $request
     */
    public function updateTest($test, $data, $request)
    {
        DB::transaction(function () use ($test, $data, $request){
            $test->update($data);
            if (request()->hasFile('image'))
            {
                $this->storeImage($test, $request);
            }
        });
    }

    public function deleteTest($test)
    {
        $test->delete();
    }
}
