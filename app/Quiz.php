<?php

namespace App;

use App\Helpers\GeneralTrait;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class Quiz extends Model
{
    use GeneralTrait;
    protected $fillable = [
        'name',
        'description',
        'quizable_id',
        'image'
    ];

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

    public function files()
    {
        return $this->belongsToMany(File::class);
    }

    public function quizable()
    {
        return $this->morphTo();

    }

    public function getSubjectQuizzes()
    {
        return $this->with('quizable')->where('quizable_type', 'subjects')->latest()->paginate(10);
    }

    public function getJobQuizzes()
    {
        return $this->with('quizable')->where('quizable_type', 'jobs')->latest()->paginate(10);
    }

    public function getTestQuizzes()
    {
        return $this->with('quizable')->where('quizable_type', 'tests')->latest()->paginate(10);
    }

    public function createSubjectQuiz($request)
    {
        $this->name = $request->name;
        $this->description = $request->description;
        $this->image = $request->image->store('uploads', 'public');
        $subject = Subject::where('id', $request->quizable_id)->first();

        DB::transaction(function () use($subject, $request) {
            $quiz = $subject->quizzes()->save($this);
            $this->storeImage($quiz, $request);
            $quiz->files()->attach($request->file);
        });
    }

    public function updateSubjectQuiz($request, $quiz)
    {

        DB::transaction(function () use($request, $quiz) {
            $quiz->update($request->all());
            if (request()->hasFile('image'))
            {
                $this->storeImage($quiz, $request);
            }
            $quiz->files()->syncWithoutDetaching($request->file);
        });

    }

    public function createJobQuiz($request)
    {

        $job = Job::where('id', $request->quizable_id)->first();

        $this->name = $request->name;
        $this->description = $request->description;
        $this->image = $request->image->store('uploads', 'public');

        DB::transaction(function () use($job, $request) {
            $quiz = $job->quizzes()->save($this);
            $this->storeImage($quiz, $request);
            $quiz->files()->attach(array_unique($request->file));
        });
    }

    public function updateJobQuiz($request, $quiz)
    {
        DB::transaction(function () use($request, $quiz) {
            $quiz->update($request->all());
            if ($request->has('file'))
            {
                $quiz->files()->syncWithoutDetaching(array_unique($request->file));
            }
            if (request()->hasFile('image'))
            {
                $this->image = $request->image->store('uploads', 'public');
                $this->storeImage($quiz, $request);
            }
        });

    }

    public function createTestQuiz($request)
    {
        $sub_category = SubCategory::where('id', $request->quizable_id)->first();

        $this->name = $request->name;
        $this->description = $request->description;
        $this->image = $request->image->store('uploads', 'public');

        DB::transaction(function () use($sub_category, $request) {
            $quiz = $sub_category->quizzes()->save($this);
            $this->storeImage($quiz, $request);
            $quiz->files()->attach(array_unique($request->file));
        });
    }

    public function updateTestQuiz($request, $quiz)
    {

        DB::transaction(function () use($quiz, $request) {
            $quiz->update($request->all());
            if ($request->has('file'))
            {
                $quiz->files()->syncWithoutDetaching(array_unique($request->file));
            }
            if (request()->hasFile('image'))
            {
                $this->storeImage($quiz, $request);
            }
        });

    }
}
