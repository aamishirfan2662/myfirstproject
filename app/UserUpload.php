<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUpload extends Model
{
    protected $guarded = [];


    public function getRouteKeyName()
    {
        return 'name';
    }
    public function createUserUpload()
    {
        $data = request()->validate([
            'name' => 'required|max:100',
            'file' => 'required'
        ]);

        $this->name = $data['name'];
        $this->file = $data['file']->store('user_uploads', 'public');

        return $this->save();
    }

    public function getUserUploads()
    {
        return $this->latest()->get();
    }

    public function destroyUserUpload($userupload)
    {
        return $userupload->delete();
    }
}
