<?php

namespace App\Observers;

use App\Blog;
use Illuminate\Support\Str;

class BlogObserver
{
    /**
     * Handle the blog "created" event.
     *
     * @param  \App\Blog  $blog
     * @return void
     */
    public function created(Blog $blog)
    {
//        $blog->update([
//            'slug' => $blog->id.'-'.Str::slug($blog->title, '-'),
//            'image' => request()->image->store('blogs', 'public')
//        ]);
    }

    /**
     * Handle the blog "updated" event.
     *
     * @param  \App\Blog  $blog
     * @return void
     */
    public function updated(Blog $blog)
    {
        $blog->update([
            'slug' => Str::slug($blog->title, '-'),
        ]);
    }

    /**
     * Handle the blog "deleted" event.
     *
     * @param  \App\Blog  $blog
     * @return void
     */
    public function deleted(Blog $blog)
    {
        //
    }

    /**
     * Handle the blog "restored" event.
     *
     * @param  \App\Blog  $blog
     * @return void
     */
    public function restored(Blog $blog)
    {
        //
    }

    /**
     * Handle the blog "force deleted" event.
     *
     * @param  \App\Blog  $blog
     * @return void
     */
    public function forceDeleted(Blog $blog)
    {
        //
    }
}
