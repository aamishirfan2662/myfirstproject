<?php

namespace App\Observers;

use App\Subject;

class SubjectObserver
{
    /**
     * Handle the subject "created" event.
     *
     * @param  \App\Subject  $subject
     * @return void
     */
    public function created(Subject $subject)
    {
        //
    }

    /**
     * Handle the subject "updated" event.
     *
     * @param  \App\Subject  $subject
     * @return void
     */
    public function updated(Subject $subject)
    {
        //
    }

    /**
     * Handle the subject "deleted" event.
     *
     * @param  \App\Subject  $subject
     * @return void
     */
    public function deleted(Subject $subject)
    {
        foreach ($subject->files as $file)
        {
            if ($file->questions)
            {
                if ($file->questions->count() > 0)
                {
                    foreach ($file->questions as $question)
                    {
                        $question->answers()->delete();
                    }
                    $file->questions()->delete();
                }
            }
            $file->quizzes()->detach();
        }
        $subject->files()->delete();

        if ($subject->quizzes)
        {
            if ($subject->quizzes->count() > 0)
            {
                $subject->quizzes()->delete();
            }
        }

    }

    /**
     * Handle the subject "restored" event.
     *
     * @param  \App\Subject  $subject
     * @return void
     */
    public function restored(Subject $subject)
    {
        //
    }

    /**
     * Handle the subject "force deleted" event.
     *
     * @param  \App\Subject  $subject
     * @return void
     */
    public function forceDeleted(Subject $subject)
    {
        //
    }
}
