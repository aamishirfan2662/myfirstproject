<?php

namespace App\Imports;

use App\Answer;
use App\File;
use App\Question;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Throwable;

class FileImport implements ToCollection
{
    private $row = 0;
    private $file_id;
    private  $answer_row = 1;

    public function __construct($file_id)
    {
        $this->file_id = $file_id;
    }

    public function collection(Collection $collection)
    {
        $question = new Question();
        $collection = $collection->chunk(5)->toArray();


        DB::transaction(function () use($collection, $question){
            foreach ($collection as $collect) {
                $answer_row = 1;

                if (sizeof($collect) == 5)
                {
                    foreach ($collect as $row) {
                        $description = null;
                        if ($this->row % 5 == 0) {

                            if ($row[0] != null){
                                if (isset($row[2]))
                                {
                                    $description = $row[2];
                                }
                                $question = Question::create([
                                    'question' => $row[0],
                                    'correct_answer' => $row[1],
                                    'description' => $description,
                                    'file_id' => $this->file_id,
                                ]);
                            }
                        } else {
                            Answer::create([
                                'answers' => Str::after($row[0], '.'),
                                'alphabet' => $this->saveAnswer($answer_row),
                                'question_id' => $question->id,
                            ]);
                            $answer_row++;
                        }

                        $this->row++;
                    }
                }

            }

        });


    }

    private function saveAnswer($answer_row)
    {
        if($answer_row  == 1)
        {
            return 'A';
        }
        else if($answer_row  == 2)
        {
            return 'B';

        }
        else if($answer_row  == 3)
        {
            return 'C';

        }
        else if($answer_row  == 4)
        {
            return 'D';

        }

    }
}