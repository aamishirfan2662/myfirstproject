<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    public function blogs()
    {
        return $this->belongsToMany(Blog::class);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * @return Tag[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getTags()
    {
        return $this->latest()->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createTag($data)
    {
        return $this->create($data);
    }

    /**
     * @param Tag $tag
     */
    public function getTagById(Tag $tag)
    {
        dd($tag);
    }

    /**
     * @param Tag $tag
     * @param $data
     * @return bool
     */
    public function updateTag(Tag $tag, $data)
    {
        return $tag->update($data);
    }

    /**
     * @param Tag $tag
     * @return bool|null
     * @throws \Exception
     */
    public function destroyTag(Tag $tag)
    {
        return $tag->delete();
    }


}
