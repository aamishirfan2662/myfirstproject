<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'name';
    }
    public function getImageAttribute($value)
    {
        if(file_exists(storage_path('app/public/'.$value)))
        {
            return  asset('storage/'.$value);
        }
        return null;
    }

    public function jobs()
    {
        return $this->hasMany(Job::class)->latest('updated_at');
    }

    public function quizzes()
    {
        return $this->morphMany(Quiz::class, 'quizable')->latest('updated_at');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }



}
