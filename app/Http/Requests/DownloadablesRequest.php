<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DownloadablesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if (!$this->route('downloadable'))
        {
            $rules += [
                'name' => 'required|max:100|'.Rule::unique('files')
                        ->where('fileable_type', 'downloadables')
                        ->where('fileable_id', $this->fileable_id)
                        ->whereNull('deleted_at'),
                'fileable_id' => 'required',
                'file' => 'required',
            ];
        }
        else
        {
            $rules += [
                'name' => 'required|max:100|'.Rule::unique('files')
                        ->ignore($this->route('downloadable'))
                        ->where('fileable_type', 'downloadables')
                        ->where('fileable_id', $this->fileable_id)
                        ->whereNull('deleted_at'),
                'fileable_id' => 'sometimes',
                'file' => 'sometimes',
            ];
        }

        return $rules;
    }
}
