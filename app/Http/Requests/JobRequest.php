<?php

namespace App\Http\Requests;

use App\Job;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sub_category_id' => 'required',
        ];

        if (!$this->route('job'))
        {

            $rules += [
                'name' => 'required|max:100|'.Rule::unique('jobs')
                        ->where('sub_category_id', $this->sub_category_id),
                'image' => 'required|mimes:png,jpg,jpeg'
            ];

        }
        else
        {
            $rules += [
                'name' => 'required|max:100|'.Rule::unique('jobs')
                        ->ignore($this->route('job'))
                        ->where('sub_category_id', $this->sub_category_id),
                'image' => 'sometimes|mimes:png,jpg,jpeg'
            ];
//            dd($rules);
        }

        return $rules;
    }
}
