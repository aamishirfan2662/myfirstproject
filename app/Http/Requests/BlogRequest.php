<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'title' => 'required|max:100',
            'excerpt' => 'required|max:100',
            'body' => 'required',
            'type' => 'required',
        ];

        if (!$this->route('blog'))
        {
            $rules += [
                'image' => 'required|mimes:png,jpg,jpeg'
            ];
        }
        else
        {
            $rules += [
                'image' => 'sometimes|mimes:png,jpg,jpeg'
            ];
        }

        return $rules;
    }
}
