<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        
        if (!$this->route('subject'))
        {

            $rules += [
                'name' => 'required|max:100|unique:subjects',
                'image' => 'required|mimes:png,jpg,jpeg'
            ];

        }
        else
        {
            $rules += [
                'name' => 'required|max:100|unique:subjects,name,'.$this->route('subject')->id,
                'image' => 'sometimes|mimes:png,jpg,jpeg'
            ];
        }
//        dd($rules);

        return $rules;
    }
}
