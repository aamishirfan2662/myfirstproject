<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobQuizRequest;
use App\Job;
use App\Quiz;
use App\SubCategory;
use App\Subject;
use Illuminate\Http\Request;

class JobQuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quizzes = new Quiz();
        $quizzes = $quizzes->getJobQuizzes();
        return view('partials.admin.job_quiz.index',[
            'quizzes' => $quizzes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiz = new Quiz();
        $subjects = new Subject();
        $subCategories = SubCategory::with('jobs')->has('jobs')->get();

        $subjects = Subject::has('files')->get();

        return view('partials.admin.job_quiz.create', [
            'quiz' => $quiz,
            'subjects' => $subjects,
            'subCategories' => $subCategories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobQuizRequest $request)
    {
        $quiz = new Quiz();
//        dd(request()->all());
        $quiz->createJobQuiz($request);

        return redirect(route('job-quizzes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Quiz $job_quiz
     * @return void
     */
    public function show($job, Quiz $job_quiz)
    {
        $job = Job::find($job);
//        dd(Quiz::where('name', $job_quiz->name)
//            ->where('quizable_id', $job->id)
//            ->where('quizable_type', 'jobs')->first());
        $subjects = new Subject();
        $subjects = Subject::has('files')->get();
        $subCategories = SubCategory::with('jobs')->get();

        return view('partials.admin.job_quiz.show', [
            'subjects' => $subjects,
            'quiz' => Quiz::where('name', $job_quiz->name)
                ->where('quizable_id', $job->id)
                ->where('quizable_type', 'jobs')->first(),
            'subCategories' => $subCategories,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Quiz $job_quiz
     * @return void
     */
    public function edit(Quiz $job_quiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobQuizRequest $request
     * @param Quiz $job_quiz
     * @return void
     */
    public function update(JobQuizRequest $request, $job_quiz)
    {
        $quiz = new Quiz();
        $quiz->updateJobQuiz($request, Quiz::find($job_quiz));
        return redirect(route('job-quizzes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Quiz $job_quiz
     * @return void
     * @throws \Exception
     */
    public function destroy($job_quiz)
    {
        $job_quiz = Quiz::find($job_quiz);

        if (file_exists(storage_path('app/public/'.$job_quiz->getOriginal('image'))))
        {
            unlink(storage_path('app/public/'.$job_quiz->getOriginal('image')));
        }
        $job_quiz->delete();
        return redirect(route('job-quizzes.index'));
    }

    public function deleteFile($job_quiz, $file_id)
    {
        $job_quiz = Quiz::find($job_quiz);
        $job_quiz->files()->detach($file_id);
        return back();
    }
}
