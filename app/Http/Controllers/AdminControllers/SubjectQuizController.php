<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectQuizRequest;
use App\Job;
use App\Quiz;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubjectQuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quizzes = new Quiz();
        $quizzes = $quizzes->getSubjectQuizzes();
        return view('partials.admin.subject_quiz.index',[
            'quizzes' => $quizzes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiz = new Quiz();
        $subjects = new Subject();
        $subjects = Subject::has('files')->get();

        return view('partials.admin.subject_quiz.create', [
            'quiz' => $quiz,
            'subjects' => $subjects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SubjectQuizRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectQuizRequest $request)
    {
        $quiz = new Quiz();
        $quiz->createSubjectQuiz($request);

        return redirect(route('subject-quizzes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Subject $subject
     * @param Quiz $subject_quiz
     * @return void
     */
    public function show(Subject $subject, Quiz $subject_quiz)
    {
        $subjects = new Subject();
        $subjects = Subject::has('files')->get();

        return view('partials.admin.subject_quiz.show', [
            'subjects' => $subjects,
            'quiz' => Quiz::where('name', $subject_quiz->name)
                ->where('quizable_id', $subject->id)
                ->where('quizable_type', 'subjects')->first(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Quiz $subject_quiz
     * @return void
     */
    public function edit(Quiz $subject_quiz)
    {
//        $subjects = new Subject();
//        $subjects = $subjects->getSubjects();
//
//        return view('partials.admin.subject_quiz.edit', [
//            'subjects' => $subjects,
//            'quiz' => $subject_quiz,
//        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Quiz $subject_quiz
     * @return void
     */
    public function update(SubjectQuizRequest $request, $subject_quiz)
    {
        $quiz = new Quiz();
        $quiz->updateSubjectQuiz($request, Quiz::find($subject_quiz));
        return redirect(route('subject-quizzes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Quiz $subject_quiz
     * @return void
     * @throws \Exception
     */
    public function destroy($subject_quiz)
    {
        $subject_quiz = Quiz::find($subject_quiz);

        if (file_exists(storage_path('app/public/'.$subject_quiz->getOriginal('image'))))
        {
            unlink(storage_path('app/public/'.$subject_quiz->getOriginal('image')));
        }
        $subject_quiz->delete();
        return redirect(route('subject-quizzes.index'));

    }

    public function deleteFile($subject_quiz, $file_id)
    {
        $subject_quiz = Quiz::find($subject_quiz);
        $subject_quiz->files()->detach($file_id);
        return back();
    }
}
