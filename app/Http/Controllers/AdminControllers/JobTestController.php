<?php

namespace App\Http\Controllers\AdminControllers;

use App\Helpers\GeneralTrait;
use App\Http\Controllers\Controller;
use App\JobTest;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class JobTestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobTest = new JobTest();
        $jobsTests = $jobTest->getJobTest();
//        dd($jobsTests);
//        foreach ($jobsTests as $j)
//        {
//            dd($j->subCategory);
//        }
        return view('partials.admin.--jobtest.index')->with('jobsTests', $jobsTests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs_test = new JobTest();
        $subCategories = SubCategory::where('category_id', '!=',null)->get();

        return view('partials.admin.--jobtest.create')->with('subCategories', $subCategories)->with('jobs_test', $jobs_test);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => Rule::unique('job_tests')->where(function ($query) use($request) {
                return $query->where('sub_category_id', $request->sub_category_id);
            }),
            'name' => 'max:100',
            'sub_category_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg'
        ]);
//        dd($data);



        $jobTest = new JobTest();
        $ds =$jobTest->createJobTest($data, $request);
//        dd($ds);
        return redirect('/admin/jobs-tests');
    }

    /**
     * Display the specified resource.
     *
     * @param JobTest $jobsTest
     * @return void
     */
    public function show(JobTest $jobsTest)
    {
        $subCategories = SubCategory::all();
        return view('partials.admin.--jobtest.show')->with('jobs_test', $jobsTest)->with('subCategories', $subCategories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobTest  $jobTest
     * @return \Illuminate\Http\Response
     */
    public function edit(JobTest $jobTest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param JobTest $jobs_test
     * @return void
     */
    public function update(Request $request, JobTest $jobs_test)
    {
        $data = request()->validate([
            'name' => Rule::unique('job_tests')->where(function ($query) use($request) {
                return $query->where('sub_category_id', $request->sub_category_id);
            }),
            'name' => 'max:100',
            'sub_category_id' => 'required',
            'image' => 'sometimes',
        ]);

        $jobs_test->updateJobTest($jobs_test, $data, $request);

        return redirect('admin/jobs-tests');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param JobTest $jobs_test
     * @return void
     */
    public function destroy(JobTest $jobs_test)
    {
        unlink(storage_path('app/public/'.$jobs_test->image));
        $jobs_test->deleteJobTest($jobs_test);

        return redirect('admin/jobs-tests');

    }
}
