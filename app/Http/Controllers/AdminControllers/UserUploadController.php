<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\UserUpload;
use Illuminate\Http\Request;

class UserUploadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $userupload = new UserUpload();

        $useruploads = $userupload->getUserUploads();

        return view('partials.admin.userupload.index')
            ->with('useruploads', $useruploads);

    }


    /**
     * @param UserUpload $userupload
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(UserUpload $userupload)
    {
        if (file_exists(storage_path('app/public/'.$userupload->file)))
        {
            unlink(storage_path('app/public/'.$userupload->file));
        }
        $userupload->destroyUserUpload($userupload);
        return redirect(route('useruploads.index'));
    }
}
