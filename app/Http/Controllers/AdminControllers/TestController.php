<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $test = new Test();
        $tests = $test->getTests();
        return view('partials.admin.test.index')->with('tests', $tests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $test = new Test();
        return view('partials.admin.test.create',[
            'test' => $test
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => 'required|unique:tests,name|max:100',
            'image' => 'required|mimes:png,jpg,jpeg'
        ]);
        $data += ['category_id' => 2];

        $test = new Test();
        $test->createTest($data, $request);
        return redirect(route('tests.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Test $test
     * @return void
     */
    public function show(Test $test)
    {
        return view('partials.admin.test.show', [
            'test' => $test,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Test $test
     * @return void
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Test $test
     * @return void
     */
    public function update(Request $request, Test $test)
    {
        $data = request()->validate([
            'name' => 'required|max:100|unique:users,name,'.$test->id,
            'image' => 'sometimes',
        ]);
        $data += ['category_id' => 2];

        $test->updateTest($test, $data, $request);

        return redirect(route('tests.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Test $test
     * @return void
     */
    public function destroy(Test $test)
    {
        unlink(storage_path('app/public/'.$test->image));
        $test->deleteTest($test);

        return redirect(route('tests.index'));

    }
}
