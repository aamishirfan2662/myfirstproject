<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Slider;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sliders = Slider::all();
        return view('partials.admin.slider.index',[
            'sliders' => $sliders
        ]);
    }

    public function update(Request $request, Slider $slider)
    {
       $request->validate([
            'primary_text' => 'sometimes|max:20',
            'secondary_text' => 'sometimes|max:30',
            'image' => 'sometimes|mimes:png,jpg,jpeg'
        ]);
       $primary_text = $request->primary_text;
       $secondary_text = $request->secondary_text;
        $slider->update([
            'primary_text' => $primary_text[0],
            'secondary_text' => $secondary_text[0],
        ]);

        if ($request->hasFile('image'))
        {
            $slider->image = $request->image->store('uploads', 'public');
            $slider->save();
        }

        return back();
    }
}
