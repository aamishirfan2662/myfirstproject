<?php

namespace App\Http\Controllers\AdminControllers;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $banner = Banner::first();
        return view('partials.admin.banner.index',[
            'banner' => $banner
        ]);
    }

    public function update(Request $request, Banner $banner)
    {
        request()->validate([
            'image' => 'sometimes|mimes:png,jpg,jpeg',
            'hyper_link' => 'required|url'
        ]);

        $banner->hyper_link = $request->hyper_link;
        $banner->save();

        if ($request->hasFile('image'))
        {
            $banner->image = $request->image->store('uploads', 'public');
            $banner->save();
        }
        return back();
    }
}
