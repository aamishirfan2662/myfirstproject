<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\UserUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DownloadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function download(UserUpload $userupload)
    {
        if (file_exists(storage_path('app/public/'.$userupload->file)))
        {
            return Storage::disk('public')->download($userupload->file);
        }

    }

    public function sample()
    {
        if (file_exists(public_path("sample.xlsx")))
        {
            return response()->download(public_path("sample.xlsx"));
        }

    }
}
