<?php

namespace App\Http\Controllers\AdminControllers;

use App\Downloadable;
use App\File;
use App\Http\Controllers\Controller;
use App\Http\Requests\FileRequest;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class FileUploadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $file = new File();
        $files = $file->getFilesWithQuizzes()->where('fileable_type', 'subjects');
        return view('partials.admin.fileupload.index')->with('files', $files);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $file = new File();
        $subject = new Subject();
        $subjects = Subject::all();
        return view('partials.admin.fileupload.create')->with('subjects', $subjects)->with('file', $file);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileRequest $request)
    {

        $file = new File();
        $file->uploadFile($request);

        return redirect(route('files.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Subject $subject
     * @param \App\File $file
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject, File $file)
    {
//        dd($file);
//        dd(File::where('fileable_id', $subject->id)->where('name', $file->name)->first());
        $subjects = Subject::all();
        return view('partials.admin.fileupload.show')
            ->with('file', File::where('fileable_id', $subject->id)->where('fileable_type', 'subjects')->where('name', $file->name)->first())
            ->with('subjects', $subjects);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FileRequest $request
     * @param Subject $subject
     * @param \App\File $file
     * @return \Illuminate\Http\Response
     */
    public function update(FileRequest $request, $file)
    {
        $f = new File();
        $f->updateFile($request, File::find($file));

        return redirect(route('files.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subject $subject
     * @param \App\File $file
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($file)
    {
        $file = File::find($file);
        if (file_exists(storage_path('app/public/'.$file->file)))
        {
            unlink(storage_path('app/public/'.$file->file));
        }
        $file->destroyFile($file);
        return redirect(route('files.index'));
    }
}
