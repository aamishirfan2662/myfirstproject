<?php

namespace App\Http\Controllers\AdminControllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\TestQuizRequest;
use App\Quiz;
use App\SubCategory;
use App\Subject;
use Illuminate\Http\Request;

class TestQuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quizzes = new Quiz();
        $quizzes = $quizzes->getTestQuizzes();
        return view('partials.admin.test_quiz.index',[
            'quizzes' => $quizzes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quiz = new Quiz();
        $subjects = new Subject();
        $subjects = Subject::has('files')->get();

        return view('partials.admin.test_quiz.create', [
            'quiz' => $quiz,
            'subjects' => $subjects,
            'subCategories' => SubCategory::where('category_id', 2)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestQuizRequest $request)
    {
        $quiz = new Quiz();
//        dd(request()->all());
        $quiz->createTestQuiz($request);

        return redirect(route('test-quizzes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param SubCategory $category
     * @param Quiz $test_quiz
     * @return void
     */
    public function show(SubCategory $category, Quiz $test_quiz)
    {
        $subjects = new Subject();
        $subjects = Subject::has('files')->get();

        return view('partials.admin.test_quiz.show', [
            'subjects' => $subjects,
            'subCategories' => SubCategory::where('category_id', 2)->get(),
            'quiz' => Quiz::where('name', $test_quiz->name)
                ->where('quizable_id', $category->id)
                ->where('quizable_type', 'tests')->first(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Quiz $job_quiz
     * @return void
     */
    public function edit(Quiz $job_quiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Quiz $test_quiz
     * @return void
     */
    public function update(TestQuizRequest $request, $test_quiz)
    {
        $quiz = new Quiz();
        $quiz->updateTestQuiz($request, Quiz::find($test_quiz));
        return redirect(route('test-quizzes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Quiz $test_quiz
     * @return void
     * @throws \Exception
     */
    public function destroy($test_quiz)
    {
        $test_quiz = Quiz::find($test_quiz);
        if (file_exists(storage_path('app/public/'.$test_quiz->getOriginal('image'))))
        {
            unlink(storage_path('app/public/'.$test_quiz->getOriginal('image')));
        }
        $test_quiz->delete();
        return redirect(route('test-quizzes.index'));
    }

    public function deleteFile($test_quiz, $file_id)
    {
        $test_quiz = Quiz::find($test_quiz);
        $test_quiz->files()->detach($file_id);
        return back();
    }
}
