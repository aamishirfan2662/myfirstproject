<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobRequest;
use App\Job;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Psy\Util\Str;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job = new Job();
        $jobs = $job->getJobs();
        return view('partials.admin.job.index')->with('jobs', $jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $job = new Job();
        $subCategories = SubCategory::where('category_id', 1)->get();

        return view('partials.admin.job.create')->with('subCategories', $subCategories)->with('job', $job);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $job = new Job();
        $job->createJob($request);
//        dd($ds);
        return redirect(route('jobs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param SubCategory $subCategory
     * @param Job $job
     * @return void
     */
    public function show(SubCategory $subCategory, Job $job)
    {
//        dump($subCategory);
//        dump($job);
//        dd(Job::where('name', $job->name)->where('sub_category_id', $subCategory->id)->first());
        $subCategories = SubCategory::where('category_id', 1)->get();
        return view('partials.admin.job.show',[
            'job' => Job::where('name', $job->name)->where('sub_category_id', $subCategory->id)->first(),
            'subCategories' => $subCategories,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Job $job
     * @return void
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobRequest $request
     * @param SubCategory $subCategory
     * @param Job $job
     * @return void
     */
    public function update(JobRequest $request , $job)
    {
        $j = new Job();
        $j->updateJob($request, Job::find($job));

        return redirect(route('jobs.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SubCategory $subCategory
     * @param Job $job
     * @return void
     */
    public function destroy($job)
    {
        $job = Job::find($job);

        if (file_exists(storage_path('app/public/'.$job->getOriginal('image'))))
        {
            unlink(storage_path('app/public/'.$job->getOriginal('image')));
        }
        $job->deleteJob($job);

        return redirect(route('jobs.index'));

    }
}
