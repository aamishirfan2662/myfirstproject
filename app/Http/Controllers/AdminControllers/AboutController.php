<?php

namespace App\Http\Controllers\AdminControllers;

use App\About;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $about = About::first();
        return view('partials.admin.about.index',[
            'about' => $about,
        ]);
    }

    public function update(Request $request, About $about)
    {
        $data = $request->validate([
            'primary_text' => 'required|max:100',
            'secondary_text' => 'required'
        ]);
        $about->update($data);

        return back();
    }
}
