<?php

namespace App\Http\Controllers\AdminControllers;

use App\Category;
use App\Downloadable;
use App\File;
use App\Http\Controllers\Controller;
use App\Http\Requests\DownloadablesRequest;
use App\SubCategory;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;

class DownloadableController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $downloadable = new Downloadable();
        $downloadable = $downloadable->getFiles();
        return view('partials.admin.downloadablefileupload.index')->with('files', $downloadable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $file = new File();
        $downloadables = Downloadable::get();
        return view('partials.admin.downloadablefileupload.create')->with('file', $file)->with('downloadables', $downloadables);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DownloadablesRequest $request)
    {
        $downloadable = new Downloadable();
        $downloadable->uploadFile($request);

        return redirect(route('downloadables.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @param File $downloadable
     * @return void
     */
    public function show(Downloadable $category, File $downloadable)
    {
        $downloadables = Downloadable::get();
        return view('partials.admin.downloadablefileupload.show', [
            'downloadables' => $downloadables,
            'file' => File::where('fileable_id', $category->id)->where('fileable_type', 'downloadables')->where('name', $downloadable->name)->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Category $category
     * @param File $downloadable
     * @return void
     */
    public function update(DownloadablesRequest $request, $downloadable)
    {
        $downloadables = new Downloadable();
        $downloadables->updateFile($request, File::find($downloadable));

        return redirect(route('downloadables.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @param File $downloadable
     * @return void
     * @throws \Exception
     */
    public function destroy($downloadable)
    {
        $downloadable = File::find($downloadable);
        if(file_exists(storage_path('app/public/'.$downloadable->file)))
        {
            unlink(storage_path('app/public/'.$downloadable->file));
        }
        $downloadable->destroyFile($downloadable);
        return redirect(route('downloadables.index'));
    }
}
