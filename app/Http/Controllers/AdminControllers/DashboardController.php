<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->is_admin == 0)
        {
            return redirect('/');
        }

        $subCategories = SubCategory::all();

        return view('layouts.admin')->with('subCategories', $subCategories);
    }
}
