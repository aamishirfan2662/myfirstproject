<?php

namespace App\Http\Controllers\AdminControllers;

use App\Blog;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = new Blog();
        $blogs = $blog->getBlogs();
        return view('partials.admin.blog.index')->with('blogs', $blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blog = new Blog();
        $tag = new Tag();
        $tags = $tag->getTags();

        return view('partials.admin.blog.create')->with('blog', $blog)->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = new Blog();
        $blog->createBlog($request);

        return redirect(route('blogs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        $tags = Tag::all();
        return view('partials.admin.blog.show')->with('blog', $blog)->with('tags', $tags);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, Blog $blog)
    {
        $blog->updateBlog($blog, $request);
        return redirect(route('blogs.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        if (file_exists(storage_path('app/public/'.$blog->getOriginal('image'))))
        {
            unlink(storage_path('app/public/'.$blog->getOriginal('image')));
        }
        $blog->deleteBlog($blog);
        return redirect(route('blogs.index'));

    }
}
