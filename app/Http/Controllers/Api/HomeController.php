<?php

namespace App\Http\Controllers\Api;

use App\Blog;
use App\Category;
use App\Downloadable;
use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Subject;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $sub_categories = SubCategory::get();
        $subjects = Subject::get();
//        $blogs = Blog::with(['tags', 'comments'])->get();
//        $downloadables = Downloadable::with('files')->where('fileable_type', 'downloadables')->has('files')->get();

        return response([
            'status' => 'success',
            'data' => [
                'sub_categories' => $sub_categories,
                'subjects' => $subjects,
            ],
        ]);
    }

    public function categories(){
        $categories = Category::get();
        return collect([
            'status' => true,
            'data' => $categories
        ]);
    }
}
