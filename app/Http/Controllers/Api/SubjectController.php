<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::get();

        if($subjects->count() > 0)
        {
            return response([
                'status' => true,
                'data' => $subjects,
            ]);
        }
        else
        {
            return response([
                'status' => false,
                'message' => 'no subjects available',
            ]);
        }

    }

    public function quizzes($id)
    {
        $subject = Subject::with('quizzes')->where('id', '=' ,$id)->first();
        if (isset($subject))
        {
            if(count($subject->quizzes) < 0)
            {
                return response([
                    'status' => false,
                    'message' => 'no quizzes available',
                ]);
            }
            return response([
                'status' => true,
                'data' => $subject->quizzes,
            ]);
        }
        return response([
            'status' => false,
            'message' => 'This subject does not exist any more',
        ]);

    }
}
