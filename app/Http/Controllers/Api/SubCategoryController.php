<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Subject;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $sub_categories = SubCategory::get();

        if ($sub_categories->count() > 0)
        {
            return response([
                'status' => true,
                'data' => $sub_categories,
            ]);
        }
        else
        {
            return response([
                'status' => false,
                'message' => 'no sub-categories available',
            ]);
        }

    }
}
