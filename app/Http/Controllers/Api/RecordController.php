<?php

namespace App\Http\Controllers\Api;

use App\ConciseRecord;
use App\DetailedRecord;
use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;

class RecordController extends Controller
{
    public function store(Request $request)
    {
        $concise = json_decode($request->concise);
        $detail = json_decode($request->detail);
        $data = DB::transaction(function () use ($request, $concise ,$detail) {
            // which will be shown in top bar
            $concise_record = ConciseRecord::create([
                'total_questions' => $concise->total_questions,
                'correct_answers' => $concise->correct_answers,
                'user_id' => $concise->user_id,
            ]);

            foreach ($detail as $object)
            {
                // for later use
                DetailedRecord::create([
                    'quiz_name' => $object->quiz_name,
                    'question' => $object->question,
                    'correct_answer' => $object->correct_answer,
                    'answered' => $object->answered,
                    'user_id' => $object->user_id
                ]);
            };
            if(!is_null($concise_record)) {
                return collect([
                    'status' => true,
                    'data' => $concise_record
                ]);
            } else {
                return collect([
                    'status' => false,
                    'data' => ''
                ]);
            }
        });
        if ($data['status'] == false)
        {
            return response([
                'status' => false,
                'message' => 'record not saved'
            ]);
        } else {
            return response([
                'status' => true,
                'data' => $data['data']
            ]);
        }
    }

}
