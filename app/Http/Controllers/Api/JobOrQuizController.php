<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\SubCategory;
use Illuminate\Http\Request;

class JobOrQuizController extends Controller
{
    public function getJobsOrQuizzesUponSubcategory($id)
    {
        $sub_category = SubCategory::where('id','=',$id)->with(['category','jobs','quizzes'])->first();
        if ($sub_category)
        {
            if ($sub_category->category->id == 1)
            {
                if (isset($sub_category->jobs) && $sub_category->jobs->count() < 1)
                {
                    return response([
                        'status' => false,
                        'message' => 'no jobs in this category',
                    ]);
                }
                return response()->json([
                    'status' => true,
                    'data' => $sub_category->jobs,
                ]);
            }
            elseif ($sub_category->category->id == 2)
            {
                if ($sub_category->quizzes->count() < 1)
                {
                    return response([
                        'status' => false,
                        'message' => 'no quizzes in this category'
                    ]);
                }
                return response()->json([
                    'status' => true,
                    'data' => $sub_category->quizzes,
                ]);
            }
        }
        return response([
            'status' => false,
            'message' => 'This category does not exist any more'
        ]);

    }
}
