<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Job;
use Illuminate\Http\Request;

class JobQuizController extends Controller
{
    public function index($id)
    {
        $job = Job::find($id);
        if(isset($job) && $job->quizzes->count() > 0)
        {
            return response([
                'status' => true,
                'data' => $job->quizzes,
            ]);
        }
        else
        {
            return response([
                'status' => false,
                'message' => 'no quizzes available',
            ]);
        }
    }
}
