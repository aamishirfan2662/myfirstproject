<?php

namespace App\Http\Controllers\Api;

use App\Contact;
use App\Events\BugReportedEvent;
use App\Http\Controllers\Controller;
use App\Mail\BugMail;
use App\Quiz;
use Illuminate\Http\Request;

class BugController extends Controller
{
    public function report(Request $request)
    {
        $quiz = Quiz::find($request->quiz);

        if ($quiz)
        {
            $data = [
                'bug' => $request->bug,
                'quiz' => $quiz->name,
                'category' => $quiz->quizable->name,
                'question' => $request->question
            ];
            $email = Contact::first()->email;
            if ($email != 'not specified') {
                event(new BugReportedEvent($data));

                return response()->json([
                    'status' => true,
                    'message' => 'Bug reported successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'Bug not reported'
            ]);
        }

    }
}
