<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Job;
use App\SubCategory;
use App\Subject;
use Illuminate\Http\Request;

class JobController extends Controller
{

    // for website work

    public function getJobs($id)
    {
        $jobs = Job::where('sub_category_id', $id)->get();
        return response()->json([
            'data' => $jobs,
        ]);
    }
}
