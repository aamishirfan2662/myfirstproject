<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\JobTest;
use App\Question;
use App\Quiz;
use http\Env\Response;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index($id)
    {
        $quiz = Quiz::with('files')->where('id', $id)->first();

        $total_files = [];
        if ($quiz)
        {
            foreach ($quiz->files as $file)
            {
                array_push($total_files, $file->id);
            }
        }

        $questions = Question::with('answers')->whereIn('file_id', $total_files)->paginate(1);
        if ($questions->count() < 0)
        {
            return response([
                'status' => false,
                'message' => 'no questions in this quiz'
            ]);
        }

        return response()->json([
            'status' => true,
            'data' => $questions
        ]);
    }
}
