<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Quiz;
use Illuminate\Http\Request;

class QuizDetailController extends Controller
{
    public function show($id)
    {
        $quiz = Quiz::find($id);

        if($quiz)
        {
            return response([
                'status' => true,
                'data' => $quiz,
            ]);
        }
        else
        {
            return response([
                'status' => false,
                'message' => 'no quiz found',
            ]);
        }
    }
}
