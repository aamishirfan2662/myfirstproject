<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function getSubjectFiles($subject)
    {
        $subject = Subject::with('files')->where('id', $subject)->first();
        return response()->json($subject->files);
    }

}
