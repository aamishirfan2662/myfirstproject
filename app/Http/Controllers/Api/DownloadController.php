<?php

namespace App\Http\Controllers\Api;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function download($id)
    {
        $downloadable = File::where([
            'id' => $id,
            'downloadable' => 1
        ])->first();

        if(! $downloadable)
        {
            return response([
                'status' => 'failed',
                'data' => [
                    'message' => 'download failed! no file exists'
                ]
            ]);
        }

        return Storage::disk('public')->download($downloadable->file, $downloadable->name.'.'.pathinfo(public_path($downloadable->file))['extension']);
    }
}
