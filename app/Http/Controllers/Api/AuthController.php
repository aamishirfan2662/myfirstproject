<?php

namespace App\Http\Controllers\Api;

use App\ConciseRecord;
use App\Http\Controllers\Controller;
use App\User;
use Firebase\JWT\JWT;
use http\Env\Response;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function userRecord(){
        $userId = (\Auth::user()->id);
        $totalQuestions = ConciseRecord::where('user_id', '=', $userId)->sum('total_questions');
        $correctAnswers = ConciseRecord::where('user_id', '=', $userId)->sum('correct_answers');
        $avgCorrectAnswers = ConciseRecord::where('user_id', '=', $userId)->avg('correct_answers');
        return collect([
           'status' => true,
           'totalQuestions' => $totalQuestions,
           'correctAnswers' => $correctAnswers,
           'avgCorrectAnswers' => $avgCorrectAnswers,
        ]);
    }

    public function register(Request $request)
    {
        $data = $request->all();
        if($data['name'] == '') {
            return collect([
                'status' => false,
                'message'=> 'name field required'
            ]);
        } else if($data['email'] == '') {
            return collect([
                'status' => false,
                'message' => 'email field required'
            ]);
        } else if($data['password'] == '') {
            return collect([
                'status' => false,
                'message' => 'password field required'
            ]);
        }

        $data['password'] = bcrypt($request->password);

        $user = User::create($data);

        if (!$user)
        {
            return response([
                'status' => false,
                'message' => 'registration failed'
            ]);
        }
        $access_token = $this->generateAccessToken($user);
        return response([
            'status' => true,
            'data' => $user,
            'token' => $access_token
        ]);
    }

    public function login(Request $request)
    {
        $data = $request->all();
        if($data['email'] == '') {
            return collect([
                'status' => false,
                'message'=> 'email field required'
            ]);
        } else if($data['password'] == '') {
            return collect([
                'status' => false,
                'message' => 'password field required'
            ]);
        }
        $emailExist = $this->checkEmailExist($data['email']);
        if($emailExist != true) {
            return collect([
                'status' => false,
                'error'=> 'email does\'t exit'
            ]);
        }
        if(!$request->requestFromLoggedInWeb) {
            $checkMatchPassword = $this->checkPasswordMatch($data['password'], $data['email']);
            if ($checkMatchPassword != true) {
                return collect([
                    'status' => false,
                    'message' => 'password not match please try again'
                ]);
            }
            if (!auth()->attempt($data)) {
                return response([
                    'status' => 'failed',
                    'data' => [
                        'message' => 'invalid credentials'
                    ]
                ]);
            }
        } else {
            Auth::loginUsingId($request->id);
        }

        $access_token = $this->generateAccessToken(auth()->user());
        return response([
            'status' => 'success',
            'data' => auth()->user(),
            'token' => $access_token
        ]);

    }

    public function checkEmailExist($email){
        $user = User::where('email','=',$email)->first();
        if(!is_null($user)) {
            return true;
        } else {
            return false;
        }
    }

    public function checkPasswordMatch($password,$email){
        $user = User::where('email','=',$email)->first();
        if(Hash::check($password,$user->password)){
            return true;
        } else {
            return false;
        }
    }

    public function me(Request $request)
    {
        $user = \Auth::user();
        if(isset($user)) {
            return response([
                'status' => true,
                'data' => auth()->user()
            ]);
        } else {
            return response([
                'status' => false,
                'data' => null
            ]);
        }
    }

    public function generateAccessToken($user)
    {
        return $user->createToken('auth_token')->accessToken;
    }

    public function forgot_password(Request $request)
    {
        if ($request->email == '')
        {
            return collect([
                'status' => false,
                'message'=> 'email field required'
            ]);
        }
        else{
            return $this->sendResetLinkEmail($request);
        }
    }

    public function sendResetLinkEmail(Request $request)
    {
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse()
            : $this->sendResetLinkFailedResponse();
    }


    protected function sendResetLinkFailedResponse()
    {
        return collect([
            'status' => false,
            'message'=> 'password reset request not completed'
        ]);
    }

    protected function sendResetLinkResponse()
    {
        return collect([
            'status' => true,
            'message'=> 'reset link sent successfully'
        ]);
    }


}
