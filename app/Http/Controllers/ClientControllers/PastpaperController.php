<?php

namespace App\Http\Controllers\ClientControllers;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PastpaperController extends Controller
{
    public function index()
    {
        $blogs = Blog::type('past papers')->get();
        return view('partials.client._blog.base')
            ->with('blogs', $blogs);
    }

    public function show(Blog $blog)
    {
        return view('partials.client._blogdetail.base')
            ->with('blog', $blog);
    }
}
