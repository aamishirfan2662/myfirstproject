<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;
use App\Quiz;
use App\Subject;
use Illuminate\Http\Request;

class AttemptController extends Controller
{
    public function index(Request $request, Quiz $quiz)
    {
        $quiz = Quiz::find($request->quiz_id);
        return view('partials.client.quizpage.base', [
            'quiz' => $quiz,
            'mode' => $request->mode,
        ]);
    }
}
