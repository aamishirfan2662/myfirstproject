<?php

namespace App\Http\Controllers\ClientControllers;

use App\About;
use App\Category;
use App\Http\Controllers\Controller;

use App\Tag;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $about = About::first();
        return view('partials.client._about.base',[
            'about' => $about
        ]);
    }
}
