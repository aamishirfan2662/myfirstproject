<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;
use App\Job;
use App\Quiz;
use App\SubCategory;
use Illuminate\Http\Request;

class JobQuizController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param SubCategory $subCategory
     * @param Job $job
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory, Job $job)
    {
        return view('partials.client._jobsquiz.base', [
            'job' => $subCategory->jobs->where('name', $job->name)->first(),
        ]);
    }

    public function detail(SubCategory $subCategory, Job $job, Quiz $quiz)
    {
        $quiz = $subCategory->jobs->where('name', $job->name)->first()->quizzes->where('name', $quiz->name)->first();
        return view('partials.client._coursedetail.base', [
            'quiz' => $quiz,
            'job' => $job,
        ]);
    }
}
