<?php

namespace App\Http\Controllers\ClientControllers;

use App\Category;
use App\Http\Controllers\Controller;

use App\SubCategory;
use App\Tag;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function jobs(SubCategory $subCategory)
    {
//        dd($categories->where('id', $subCategory->category_id)->first()->subCategories);
        return view('partials.client._joblist.base')
            ->with('subCategory', $subCategory);
    }
}
