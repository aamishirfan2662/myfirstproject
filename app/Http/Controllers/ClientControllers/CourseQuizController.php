<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;
use App\JobTest;
use App\Quiz;
use Illuminate\Http\Request;

class CourseQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobTest  $jobTest
     * @return \Illuminate\Http\Response
     */
    public function show(JobTest $jobTest)
    {
        return view('partials.client.courselist.base', [
            'jobTest' => $jobTest,
        ]);
    }

    public function detail(JobTest $jobTest, Quiz $quiz)
    {
//        dd($jobTest->subCategory);
        return view('partials.client._coursedetail.base', [
            'quiz' => $quiz,
            'jobTest' => $jobTest,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobTest  $jobTest
     * @return \Illuminate\Http\Response
     */
    public function edit(JobTest $jobTest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobTest  $jobTest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobTest $jobTest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobTest  $jobTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobTest $jobTest)
    {
        //
    }
}
