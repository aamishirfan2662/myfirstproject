<?php

namespace App\Http\Controllers\ClientControllers;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScholarshipController extends Controller
{
    public function index()
    {
        $blogs = Blog::type('scholarships')->latest()->get();
        return view('partials.client._blog.base')
            ->with('blogs', $blogs);
    }

    public function show(Blog $blog)
    {
        return view('partials.client._blogdetail.base')
            ->with('blog', $blog);
    }
}
