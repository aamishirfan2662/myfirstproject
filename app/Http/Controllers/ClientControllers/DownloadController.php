<?php

namespace App\Http\Controllers\ClientControllers;

use App\Category;
use App\Downloadable;
use App\File;
use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;

class DownloadController extends Controller
{

    public function show(Downloadable $downloadable)
    {
        $files = $downloadable->getFiles()->whereIn('fileable_id', $downloadable->id);

        return view('partials.client.downloadables.index')
            ->with('files', $files)->with('category', $downloadable->category);
    }
    public function download($downloadable)
    {
        $downloadable = File::find($downloadable);
        return Storage::disk('public')->download($downloadable->file, $downloadable->name.'.'.pathinfo(public_path($downloadable->file))['extension']);
    }

    public function sample()
    {
        return response()->download(public_path("sample.xlsx"));
    }

}
