<?php

namespace App\Http\Controllers\ClientControllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function showBlog(Tag $tag)
    {
        return view('partials.client._taggedblog.base')
            ->with('tag', $tag);
    }

    public function showNews(Tag $tag)
    {
        return view('partials.client._taggednews.base')
            ->with('tag', $tag);
    }
}
