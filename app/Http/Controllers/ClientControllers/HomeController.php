<?php

namespace App\Http\Controllers\ClientControllers;

use App\Blog;
use App\Category;
use App\Http\Controllers\Controller;
use App\Slider;
use App\Tag;
use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
//        $subCategories = SubCategory::with('files')->has('files')->get();
//        dd($subCategories);
//        foreach ($subCatego   ries as $subCategory)
//        {
//            if($subCategory->files->isEmpty())
//            {
//                continue;
//            }
//            else
//            {
//                dd($subCategory->name);
//            }
//        }
//        $count=0;
        $blogs = Blog::with('tags')->where('type', 'blog')->latest()->take(3)->get();
        $news = Blog::with('tags')->where('type', 'news')->latest()->take(3)->get();

//        foreach ($categories as $category)
//        {
//            dd($category->subCategories);
//        }
//        dd($count);
//        $c = Category::with('subCategories.jobsTests.quizzes.questions.questions.answers')->get();
//        dd($c);
        return view('partials.client._home.base',[
            'blogs' => $blogs,
            'news' => $news,
            'slides' => Slider::get()
        ]);
//        return view('index1');
    }
}
