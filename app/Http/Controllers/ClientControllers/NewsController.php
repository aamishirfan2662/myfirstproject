<?php

namespace App\Http\Controllers\ClientControllers;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
//        $tags = Tag::with('blogs')->get();
        $news = Blog::type('news')->latest()->get();
        return view('partials.client._news.base')
            ->with('news', $news);
    }

    public function show(Blog $news)
    {
//        $comments = Comment::with('replies')->where('parent_id', 0)->get();
        return view('partials.client._newsdetail.base')
            ->with('news', $news);
//            ->with('comments', $comments);
    }
}
