<?php

namespace App\Http\Controllers\ClientControllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Tag;
use App\UserUpload;
use Illuminate\Http\Request;

class UserUploadController extends Controller
{
    public function create()
    {
        return view('partials.client._userupload.create');
    }

    public function store()
    {
        $userupload = new UserUpload();
        $userupload->createUserUpload();

        session()->flash('message', "Your file is uploaded successfully!");

        return redirect(route('useruploads.create'));
    }
}
