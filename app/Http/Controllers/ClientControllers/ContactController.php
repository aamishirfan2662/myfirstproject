<?php

namespace App\Http\Controllers\ClientControllers;

use App\Category;
use App\Contact;
use App\Http\Controllers\Controller;

use App\Mail\ContactMail;
use App\Tag;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\Queue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        $contact = Contact::first();
        return view('partials.client._contact.base',[
            'contact' => $contact
        ]);
    }

    public function email(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'message' => 'required',
        ]);

        Mail::to('learnrel.team@gmail.com')->send(new ContactMail($data));
        return redirect(route('contact'));

    }
}
