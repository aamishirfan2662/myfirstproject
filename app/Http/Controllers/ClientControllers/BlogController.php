<?php

namespace App\Http\Controllers\ClientControllers;

use App\Blog;
use App\Category;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Tag;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::type('blog')->latest()->get();
        return view('partials.client._blog.base')
            ->with('blogs', $blogs);
    }

    public function show(Blog $blog)
    {
        return view('partials.client._blogdetail.base')
            ->with('blog', $blog);
    }


}
