<?php

namespace App\Http\Controllers\ClientControllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Quiz;
use App\SubCategory;
use App\Test;
use Illuminate\Http\Request;

class TestQuizController extends Controller
{
    public function quizzes(SubCategory $subCategory)
    {
        $quizzes = Quiz::with('files')->where('quizable_type', 'tests')->where('quizable_id', $subCategory->id)->latest()->get();
        return view('partials.client._tests.base')
            ->with('quizzes', $quizzes);
    }

    public function detail(SubCategory $subCategory, Quiz $quiz)
    {
        $quiz = $subCategory->quizzes->where('name', $quiz->name)->first();
        return view('partials.client._coursedetail.base', [
            'quiz' => $quiz,
        ]);
    }
}
