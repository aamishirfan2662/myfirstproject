<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class);
    }

//    public function quizzes()
//    {
//        return $this->morphMany(Quiz::class, 'quizable');
//    }
}
