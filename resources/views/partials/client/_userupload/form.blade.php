<div class="container">
    <div class="kt-portlet__body">
        <div class="form-group">
            <h6>Name</h6>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter your name" value="{{ old('name')}}">
            @error('name')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
        <div class="form-group">
            <h6 for="image">File<span><a class="font-10 ml-3" href="{{ asset('sample.xlsx') }}">( Click to download sample file )</a></span></h6>

            <input type="file" class="form-control @error('file') is-invalid @enderror" name="file">
            @error('file')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
</div>
