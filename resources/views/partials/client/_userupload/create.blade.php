@extends('layouts.client')

@section('content')
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success" id="success-alert">
                <strong class="text-white mr-2">File </strong>uploaded.
            </div>
        @endif
        <!--begin::Form-->
        <div class="container p-5">
            <form class="kt-form" action="{{ route('useruploads.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @include('partials.client._userupload.form')
                <div class="container">
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-10">
                                    <button type="submit" class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!--end::Form-->
    </div>
@endsection