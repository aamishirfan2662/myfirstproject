@extends('layouts.client')

@section('content')
  <div class="main-content">

    <!-- Section: News & Blog -->
    <section id="news" class="bg-light">
      <div class="customContainerPb">

        {{--blogs--}}
        @include('partials.client._news.news')

      </div>
    </section>
  </div>
@endsection
