<div class="section-content">
    <div class="row">
        @if($news->count() > 0)
            @foreach($news as $new)
                <div class="col-sm-6 col-md-3">
                    <article class="post mb-30 mb-sm-30">
                        <div class="entry-header">
                            <div class="post-thumb thumb">
                                <img src="{{ $new->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" alt="" class="img-responsive img-fullwidth">
                            </div>
                        </div>
                        <div class="entry-content p-20 pr-10 bg-gray-lighter">
                            <div class="entry-meta media mt-0 no-bg no-border">
                                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                    <ul>
                                        <li class="font-16 text-white font-weight-600 border-bottom">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->day}}</li>
                                        <li class="font-12 text-white text-uppercase">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->shortMonthName}}</li>
                                    </ul>
                                </div>
                                <div class="media-body pl-15">
                                    <div class="event-content pull-left flip">
                                        <h4 class="entry-title text-uppercase m-0 mt-5"><a href="{{ route('client.news.show', ['news' => $new]) }}">{{ $new->title }}</a></h4>
                                        {{--                                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>--}}
                                        {{--                                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>--}}
                                    </div>
                                </div>
                            </div>
                            <h6 class="mt-10">{{ $new->excerpt }}</h6>
                            <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('client.news.show', ['news' => $new]) }}">view more</a>
                            <div class="clearfix"></div>
                        </div>
                    </article>
                </div>
            @endforeach
        @else
            <div class="col-md-9 blog-pull-right form-group">
                <h5 class="alert alert-warning">No relevant news</h5>
            </div>
        @endif
    </div>
</div>
