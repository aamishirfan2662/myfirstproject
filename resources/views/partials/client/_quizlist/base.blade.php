@extends('layouts.client')

@section('content')
    <div class="main-content">

        <section class="customContainerPb">
            <div class="">
                <div class="row">
                    <div class="col-md-9 blog-pull-right">
                        @if($subject->quizzes->count() > 0)
                            @foreach($subject->quizzes as $quiz)
                                @if($quiz->files->count() > 0)
                                    <div class="row mb-15">
                                        <div class="col-sm-6 col-md-4">
                                            <div class="thumb"> <img alt="featured project" src="{{ $quiz->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" class="img-fullwidth"></div>
                                        </div>
                                        <div class="col-sm-6 col-md-8">
                                            <h4 class="line-bottom mt-0 mt-sm-20">{{ $quiz->name }}</h4>
                                            <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('subject.quizzes.detail', ['quiz' => $quiz, 'subject' => $subject]) }}">view detail</a>
                                        </div>
                                    </div>
                                    <hr>
                                @else
                                    <div class="col-md-9 blog-pull-right form-group">
                                        <h5 class="alert alert-warning">No quizzes available</h5>
                                    </div>
                                    @break
                                @endif
                            @endforeach
                        @else
                            <div class="col-md-9 blog-pull-right form-group">
                                <h5 class="alert alert-warning">No quizzes available</h5>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar sidebar-left mt-sm-30">
                            <div class="widget">
                                <h5 class="widget-title line-bottom">Other <span class="text-theme-color-2">Subjects</span></h5>
                                <div class="categories">
                                    <ul class="list list-border angle-double-right">
                                        @foreach($subjects as $subject)
                                            <h6><li><a href="{{ route('subject.quizzes.show', ['subject' => $subject]) }}">{{ $subject->name }}</a></li></h6>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection