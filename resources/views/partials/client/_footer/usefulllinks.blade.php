<div class="col-sm-6 col-md-3">
    <div class="widget dark">
        <h4 class="widget-title">Useful Links</h4>
        <ul class="list angle-double-right list-border">
            <li><a href="{{ route('about') }}">About Us</a></li>
            <li><a href="{{ route('contact') }}">Contact us</a></li>
            <li><a href="{{ route('client.blogs') }}">Blogs</a></li>
        </ul>
    </div>
</div>