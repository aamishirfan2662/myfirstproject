<div class="col-sm-3 col-md-6">
    <div class="widget dark">
        <h5 class="widget-title">Tags</h5>
        <div class="tags">
            @foreach($tags as $tag)
                <a href="{{ route('tagged.showBlog', ['tag' => $tag]) }}">{{ $tag->name }}</a>
            @endforeach
        </div>
    </div>
</div>