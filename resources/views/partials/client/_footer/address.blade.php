<div class="col-sm-3 col-md-3">
    <div class="widget dark">
        @if(isset($contact))
        <h6>{{ $contact->address }}</h6>
        <ul class="list-inline mt-5">
            <li class="m-0  pr-10"> <i class="fa fa-phone text-theme-color-2 mr-1"></i>{{ $contact->phone }}</li>
            <li class="m-0  pr-10"> <i class="fa fa-envelope-o text-theme-color-2 mr-1"></i>{{ $contact->email }}</li>
{{--            <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-color-2 mr-1"></i> <a class="text-gray" href="#">www.learnrelief.com</a> </li>--}}
        </ul>
        @endif
    </div>
</div>
