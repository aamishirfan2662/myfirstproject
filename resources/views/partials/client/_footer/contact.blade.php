<div class="row mt-20">
    <div class="col-md-2">
        <div class="widget dark">
            <h5 class="widget-title mb-10">Call Us Now</h5>
            <div class="text-gray">
                @if(isset($contact))
                    {{ $contact->phone }}
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="widget dark">
            <h5 class="widget-title mb-10">Connect With Us</h5>
            <ul class="styled-icons icon-bordered icon-sm">
                <li><a href="https://web.facebook.com/learntogetrelief/?ref=page_internal" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/learn_relief" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.reddit.com/user/LearnRelief" target="_blank"><i class="fa fa-reddit"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCw_9uqq4lCsbCPC6hKJ9mDg/?guided_help_flow=5" target="_blank"><i class="fa fa-youtube"></i></a></li>
                <li><a href="https://www.instagram.com/Learn_Relief/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.pinterest.com/LearnRelief/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="https://learnrelief.blogspot.com/" target="_blank"><i class="fa fa-blog"></i></a></li>
            </ul>
        </div>
    </div>
</div>
