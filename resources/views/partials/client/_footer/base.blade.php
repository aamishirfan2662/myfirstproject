<footer id="footer" class="footer divider layer-overlay overlay-dark-9" data-bg-img="{{ asset('assets/images/bg/bg2.jpg') }}">
    <div class="container">
        <div class="row border-bottom">

            <!-- Contact -->
            @include('partials.client._footer.address')

            <!-- Usefull links -->
            @include('partials.client._footer.usefulllinks')

            {{--Tags--}}
            @include('partials.client._footer.tags')

        </div>
        @include('partials.client._footer.contact')

    </div>
    <!-- Footer-bottom -->
    @include('partials.client._footer.footerbottom')
</footer>