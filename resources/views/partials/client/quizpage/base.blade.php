@extends('layouts.client')


@section('content')

    @include('partials.client._home.banner')
    <div id="quiz">
        <div class="container mt-30 mb-20" id="app">
            <div class="row justify-content-center">
                <div class="customContainerPb col-md-10">

                    <!-- Script to illustrates slideToggle() method -->
                    @if($mode == 1 && !Auth::check())
                        <div class="col-md-9 blog-pull-left form-group">
                            <h5 class="alert alert-warning">Please login to attempt quiz in test mode.</h5>
                        </div>
                    @else
                        <div class="container">
                            <!-- Trigger the modal with a button -->

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog modal-sm bg-lighter">
                                    <div class="modal-content bg-lighter">
                                        <div class="modal-header">
                                            <h5>Report</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div>
                                                <input type="radio" id="r1" class="report" name="report"
                                                       value="Typo in question" checked>
                                                <label for="mode1">Typo in question</label>
                                            </div>
                                            <div>
                                                <input type="radio" id="r2" class="report" name="report"
                                                       value="Typo in answers">
                                                <label for="mode1">Typo in answers</label>
                                            </div>
                                            <div>
                                                <input type="radio" id="r3" class="report" name="report"
                                                       value="Possibly wrong answer">
                                                <label for="mode2">Possibly wrong answer</label>
                                            </div>
                                            <div>
                                                <input type="radio" id="r4" class="report" name="report"
                                                       value="Question not clear">
                                                <label for="mode1">Question not clear</label>
                                            </div>
                                            <div>
                                                <input type="radio" id="r5" class="report" name="report"
                                                       value="Incomplete Question/Answer">
                                                <label for="mode2">Incomplete Question/Answer</label>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button v-on:click="report()" class="btn btn-primary customRegisterStyle"
                                                    style="background: #F2184E !important;border: 0px !important;"
                                                    data-dismiss="modal">Report
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <span><button type="button" data-toggle="modal" data-target="#myModal"><i
                                        class="fa text-danger fa-bug"></i></button></span>
                            <span><button type="button"><a
                                        href="https://api.whatsapp.com/send?text={{ url()->current() }}"
                                        target="window"><i class="fa text-success fa-whatsapp"></i></a></button></span>
                            <span><button v-on:click="infoToggle()" type="button"><i
                                        class="fa text-info fa-info"></i></button></span>
                            <span>
                                <button v-on:click="speak()" type="button" value="Play">
                                    <i class="fa text-warning fa-volume-up"></i>
                                </button>
                            </span>
                        </div>
                        <div v-if="!end" class="border-5px row" style="border-color: #202C45 !important">
                            <div class="card-header p-2 customBackGroundCH col-md-12">
                                <div class="inline-block col-md-5">
                                    <span
                                        class="font-weight-bold colorWhite">{{ $mode == 1 ? 'Test Mode' : 'Practice Mode' }}</span>
                                </div>
                                <div class="inline-block col-md-4">
                                    <span class="font-weight-bold colorWhite">{{ $quiz->name }}</span>
                                </div>
                            </div>
                            <div class="container mt-2">
                                <div class="container mt-4 mb-2">
                                    <h5 class="font-weight-bold">@{{ question }}</h5>
                                </div>

                                <div v-for="answer in answers" class="container">
                                    <div>
                                        <input type="radio" name="answer" :id="answer.id" class="answer"
                                               :value="answer.alphabet" @change="onSelect($event)">
                                        <label class="inline-block font-16 ml-1" :for="answer.id" :id="answer.alphabet">@{{
                                            answer.answers }}</label>
                                    </div>
                                </div>

                                <div class="container mb-1">
                                    <a href="/" class="btn btn-primary customLoginStyle" type="button"
                                       style="float: right; margin-right: 1%">
                                        <i class="fa fa-close fa-sm"></i>Close
                                    </a>
                                    <button @click="nextQuestion($event)" id="next"
                                            class="btn btn-primary customLoginStyle" type="button"
                                            style="float: right; margin-right: 1%">
                                        <i class="fa flaticon2-fast-next fa-sm"></i>Next
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div v-else class="border-5px row" style="border-color: #202C45 !important">
                            <div class="card-header p-2 customBackGroundCH col-md-12"><span
                                    class="font-weight-bold colorWhite">{{ $mode == 1 ? 'Test Mode' : 'Practice Mode' }}</span>
                            </div>
                            <div class="container mt-2">
                                <div class="container mt-4 mb-2">
                                    <h3><em>Your score is <span
                                                class="">@{{ correct_answers }}/@{{ total_questions }}</span></em></h3>
                                </div>
                            </div>
                        </div>
                        <h6 v-show="info" class="mt-4">@{{ description }}</h6>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('partials.client._home.banner')



    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        $(function () {
            var app = new Vue({


                el: '#quiz',
                data: {
                    quiz: '',
                    questions: [],
                    question: '',
                    description: '',
                    answers: [],
                    answered: '',
                    next_page: 0,
                    test_record: [],
                    correct_answer: '',
                    page: 1,
                    detail_record: [],
                    concise_record: [],
                    temp_record: '',
                    end: false,
                    total_questions: 1,
                    correct_answers: 0,
                    conQuestion: '',
                    speakQuestion: '',
                    selected: false,
                    info: false
                },

                mounted() {
                    document.getElementById('next').disabled = true
                    this.quiz = '{!! json_decode(json_encode($quiz->id)) !!}'
                    this.loginUser();
                },
                methods: {
                    loginUser: function () {
                        let user = '{{\Auth::check()}}';
                        let obj = {};

<<<<<<< HEAD
                        console.log('user');
=======
>>>>>>> 4a4502cfa85da17c29588388f7d0a6a70554ef70
                        console.log(user);
                        if(user) {
                            console.log('user logged in ');
                            obj = {
                                email: 'dummyuser@gmail.com',
                                password: '123456789',
                                id: '{{\Auth::id()}}',
                                requestFromLoggedInWeb: true
                            };
                        } else {
                            obj = {
                                email: 'dummyuser@gmail.com',
                                password: '123456789'
                            };
                        }
                        axios.post('/api/login', obj)
                            .then((response) => {
                                localStorage.setItem('token', response.data.token);
                                this.getQuiz();
                            }, (error) => {
                            });
                    },
                    getQuiz: function () {
                        let token = 'Bearer ' + localStorage.getItem('token');
                        axios.get(`/api/quiz/${this.quiz}`, { headers: { Authorization: token } })
                            .then(response => {
                                this.questions = response.data;
                                this.questions = this.questions.data
                                this.next_page = this.questions.next_page_url
                                this.correct_answer = this.questions.data[0].correct_answer
                                this.question = this.questions.data[0].question
                                this.description = this.questions.data[0].description
                                this.answers = this.questions.data[0].answers
                            })
                    },
                    disableRadio: function (toggle) {
                        let e = document.querySelectorAll('.answer')

                        e.forEach(function (radio) {
                            radio.disabled = toggle;
                        })
                    },

                    unsetColors: function () {

                        let e = document.querySelectorAll('.answer')

                        // refreshing radio buttons
                        e.forEach(function (radio) {
                            radio.checked = false;
                        })
                        document.getElementById('A').setAttribute('style', 'color:#ffffff');
                        document.getElementById('B').setAttribute('style', 'color:#ffffff');
                        document.getElementById('C').setAttribute('style', 'color:#ffffff');
                        document.getElementById('D').setAttribute('style', 'color:#ffffff');
                    },
                    onSelect: function ($event) {
                        document.getElementById('next').disabled = false
                        let correct_answer = this.correct_answer;
                        this.answered = $event.target.value

                        if (this.answered == correct_answer) {
                            document.getElementById($event.target.value).setAttribute('style', 'color: #28a745');
                            this.correct_answers = this.correct_answers + 1
                        } else {
                            document.getElementById($event.target.value).setAttribute('style', 'color: #dc3545');

                            this.answers.forEach(function (answer) {

                                if (answer.alphabet == correct_answer) {
                                    document.getElementById(answer.alphabet).setAttribute('style', 'color: #28a745');
                                }
                            })
                        }


                        if (parseInt('{!! json_decode(json_encode(Auth::check())) !!}')) {
                            this.temp_record = {
                                'quiz_name': this.quiz,
                                'question': this.question,
                                'correct_answer': this.correct_answer,
                                'answered': this.answered,
               E                 'user_id': parseInt('{!! json_decode(json_encode(Auth::id())) !!}')
                            }
                        }

                        this.detail_record.push(this.temp_record)
                        this.disableRadio(true)

                    },
                    createSpeakAble: function (question, answers) {
                        question = question + ' ';
                        for (var i = 0; i < answers.length; i++) {
                            question += (i + 1) + '.' + answers[i].answers + ' ';
                        }
                        return question;
                    },
                    nextQuestion: function ($event) {
                        document.getElementById('next').disabled = true
                        if ($event.target.innerText == 'submit') {
                            this.end = true
                            this.concise_record = {
                                'total_questions': this.total_questions,
                                'correct_answers': this.correct_answers,
                                'user_id': parseInt('{!! json_decode(json_encode(Auth::id())) !!}') ?? '0',
                            }


                            if ('{!! json_decode(json_encode(Auth::check())) !!}') {
                                if (parseInt('{!! json_decode(json_encode($mode)) !!}')) {
                                    let token = 'Bearer ' + localStorage.getItem('token');
                                    axios.post(`/api/quiz/record`, {
                                        concise: JSON.stringify(this.concise_record),
                                        detail: JSON.stringify(this.detail_record)
                                    }, {
                                        headers:  {
                                            'Authorization': token
                                        }
                                    })
                                }
                            }

                            return;
                        }
                        this.disableRadio(false)
                        this.unsetColors();

                        let token = 'Bearer ' + localStorage.getItem('token');
                        this.page = this.page + 1

                        axios.get(`/api/quiz/{!! json_decode(json_encode($quiz->id)) !!}?page=${this.page}`, { headers: { Authorization: token } })
                            .then(response => {
                                this.questions = response.data;
                                this.questions = this.questions.data
                                this.next_page = this.questions.next_page_url
                                this.speakQuestion = this.questions.data[0].question;
                                this.question = this.questions.data[0].question
                                this.description = this.questions.data[0].description
                                this.answers = this.questions.data[0].answers
                                this.correct_answer = this.questions.data[0].correct_answer

                                if (this.next_page == null) {
                                    $event.target.innerText = 'submit'
                                }


                            })
                        this.total_questions = this.total_questions + 1

                    },

                    speak: function () {
                        responsiveVoice.speak(this.createSpeakAble(this.question, this.answers))
                    },

                    infoToggle: function () {
                        this.info = !this.info
                    },

                    report: function () {
                        let e = document.querySelectorAll('.report')
                        let quiz = this.quiz
                        let question = this.question
                        e.forEach(function (bug) {
                            if (bug.checked == true) {

                                axios.post(`/api/quiz/report`, {
                                    bug: bug.value,
                                    quiz: quiz,
                                    question: question
                                })
                            }
                        })
                    }
                }
            })
        })
    </script>
@endsection
