@extends('layouts.client')

@section('content')
  <div class="main-content">

    <!-- Divider: Contact -->
    @include('partials.client._contact.contact')

  </div>
@endsection