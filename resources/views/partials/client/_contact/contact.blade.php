<section class="divider">
    <div class="container pt-0">
        <div class="row mb-60 bg-deep">
            <div class="col-sm-12 col-md-4">
                <div class="contact-info text-center pt-60 pb-60 border-right">
                    <i class="fa fa-phone font-36 mb-10 text-theme-colored"></i>
                    <h4>Call Us</h4>
                    <h6 class="text-gray">Phone: {{ $contact->phone }}</h6>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="contact-info text-center  pt-60 pb-60 border-right">
                    <i class="fa fa-map-marker font-36 mb-10 text-theme-colored"></i>
                    <h4>Address</h4>
                    <h6 class="text-gray">{{ $contact->address }}</h6>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="contact-info text-center  pt-60 pb-60">
                    <i class="fa fa-envelope font-36 mb-10 text-theme-colored"></i>
                    <h4>Email</h4>
                    <h6 class="text-gray">{{ $contact->email }}</h6>
                </div>
            </div>
        </div>
        <div class="row pt-10">
            <div class="col-md-5">
                <h4 class="mt-0 mb-30 line-bottom">Find Our Location</h4>
                <!-- Google Map HTML Codes --><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5456.163483134849!2d144.95177475051227!3d-37.81589041361766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4dd5a05d97%3A0x3e64f855a564844d!2s121+King+St%2C+Melbourne+VIC+3000%2C+Australia!5e0!3m2!1sen!2sbd!4v1556130803137!5m2!1sen!2sbd" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-md-7">
                <h4 class="mt-0 mb-30 line-bottom">Interested in discussing?</h4>
                <!-- Contact Form -->
                <form id="contact_form" name="contact_form" class="" action="{{ route('contact.email') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h6 for="form_name">Name</h6>
                                <input name="name" class="form-control" type="text" placeholder="Enter Name" required="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h6>Email <small>*</small></h6>
                                <input name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form-control email" type="email" placeholder="Enter Email">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <h6 for="form_message">Message</h6>
                        <textarea name="message" class="form-control required" rows="5" placeholder="Enter Message"></textarea>
                    </div>
                    <div class="form-group">
                        <input name="form_botcheck" class="form-control" type="hidden" value="" />
                        <button type="submit" class="btn btn-flat text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px"><h5>Send your message</h5></button>
                    </div>
                </form>

                <!-- Contact Form Validation-->
{{--                <script type="text/javascript">--}}
{{--                    $("#contact_form").validate({--}}
{{--                        submitHandler: function(form) {--}}
{{--                            var form_btn = $(form).find('button[type="submit"]');--}}
{{--                            var form_result_div = '#form-result';--}}
{{--                            $(form_result_div).remove();--}}
{{--                            form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');--}}
{{--                            var form_btn_old_msg = form_btn.html();--}}
{{--                            form_btn.html(form_btn.prop('disabled', true).data("loading-text"));--}}
{{--                            $(form).ajaxSubmit({--}}
{{--                                dataType:  'json',--}}
{{--                                success: function(data) {--}}
{{--                                    if( data.status == 'true' ) {--}}
{{--                                        $(form).find('.form-control').val('');--}}
{{--                                    }--}}
{{--                                    form_btn.prop('disabled', false).html(form_btn_old_msg);--}}
{{--                                    $(form_result_div).html(data.message).fadeIn('slow');--}}
{{--                                    setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);--}}
{{--                                }--}}
{{--                            });--}}
{{--                        }--}}
{{--                    });--}}
{{--                </script>--}}
            </div>
        </div>
    </div>
</section>