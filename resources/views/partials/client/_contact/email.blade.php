@component('mail::message')

    <label for="">Name: </label><span>{{ $data['name'] }}</span>
    <label for="">Email: </label><span>{{ $data['email'] }}</span>
<hr>
#Message:
{{ $data['message'] }}
Thanks,<br>
{{ config('app.name') }}
@endcomponent
