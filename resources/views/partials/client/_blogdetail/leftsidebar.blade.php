{{--<div class="col-sm-12 col-md-3">--}}
{{--    <div class="sidebar sidebar-left mt-sm-30">--}}
{{--        <div class="widget">--}}
{{--            <h5 class="widget-title line-bottom">Tags</h5>--}}
{{--            <div class="tag">--}}
{{--                <ul class="list list-border angle-double-right">--}}
{{--                    @foreach($tags as $tag)--}}
{{--                        <a href="{{ route('tagged.show', ['tag' => $tag]) }}">{{ $tag->name }}</a>--}}
{{--                    @endforeach--}}

{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="col-sm-6 col-md-3">
    <div class="widget">
        <h5 class="widget-title line-bottom">Tags</h5>
        <div class="tags">
            @foreach($tags as $tag)
                <h6 class="inline-block"><a href="{{ route('tagged.showBlog', ['tag' => $tag]) }}">{{ $tag->name }}</a></h6>
            @endforeach
        </div>
    </div>
</div>