<div class="col-md-9 blog-pull-right">
    <div class="blog-posts single-post">
        <article class="post clearfix mb-0">
            <div class="entry-header">
                <div class="post-thumb thumb"> <img src="{{ $blog->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" alt="" class="img-responsive img-fullwidth"> </div>
            </div>
            <div class="entry-title pt-10 pl-15">
                <h4><a class="text-uppercase" href="#">{{ $blog->title }}</a></h4>
            </div>
            <div class="entry-meta pl-15">
                <ul class="list-inline">
                    <li class="customGrey">Posted: <span class="text-theme-color-2">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)}}</span></li>
                    <li class="customGrey">By: <span class="text-theme-color-2">Admin</span></li>
{{--                    <li><i class="fa fa-comments-o ml-5 mr-5"></i> 5 comments</li>--}}
                </ul>
            </div>
            <div class="entry-content mt-10">
                <h6>{!! html_entity_decode($blog->body) !!}</h6>
                <div class="mt-30 mb-0">
{{--                    <h5 class="pull-left mt-10 mr-20 text-theme-color-2">Share:</h5>--}}
                    <ul class="styled-icons icon-circled m-0">
{{--                        <div class="fb-share-button" data-layout="button_count" data-size="small"><a target="_blank" href="{{ request()->fullUrl() }}" class="fb-xfbml-parse-ignore">Share</a></div>--}}
                        <div class="sharethis-inline-share-buttons"></div>
                    </ul>
                </div>
            </div>
        </article>
        <div class="tagline p-0 pt-20 mt-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="tags">
                        <h6 class="mb-0"><i class="fa fa-tags text-theme-color-2"></i> <span>Tags:</span>
                            @foreach($blog->tags as $tag)
                                <a href="{{ route('tagged.showBlog', ['tag' => $tag]) }}" class="alert-link">{{ $tag->name }}</a>
                            @endforeach
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.client._blogdetail.comments')
    </div>
</div>