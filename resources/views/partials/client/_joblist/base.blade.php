@extends('layouts.client')

@section('content')
    <div class="main-content">

        <section class="">
            <div class="customContainerPb">
                <div class="row">
                    @if($subCategory->jobs->count() > 0)
                    <div class="col-md-9 blog-pull-right">
                        @foreach($subCategory->jobs as $job)
                                <div class="row mb-15">
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumb"> <img alt="featured project" height="230" width="260" src="{{ $job->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'"" class="img-fullwidth"></div>
                                    </div>
                                    <div class="col-sm-6 col-md-8">
                                        <h4 class="line-bottom mt-0 mt-sm-20">{{ $job->name }}</h4>
                                        <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('job.quizzes.show', ['subCategory' => $subCategory, 'job' => $job]) }}">view courses</a>
                                    </div>
                                </div>
                                <hr>
                        @endforeach
                    </div>
                    @else
                        <div class="col-md-9 blog-pull-right form-group">
                            <h5 class="alert alert-warning">No jobs in this Category</h5>
                        </div>
                    @endif
                    <div class="col-md-3">
                        <div class="sidebar sidebar-left mt-sm-30">
                            <div class="widget">
                                <h5 class="widget-title line-bottom">Course <span class="text-theme-color-2">Categories</span></h5>
                                <div class="categories">
                                    <ul class="list list-border angle-double-right">
                                        @foreach($categories as $category)
                                            @if($category->subCategories->where('category_id', 1)->count() > 0)
                                                    @foreach($category->subCategories as $subCategory)
                                                        <h6><li><a href="{{ route('jobs.jobs', ['subCategory' => $subCategory]) }}">{{ $subCategory->name }}</a></li></h6>
                                                    @endforeach
                                            @else
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection