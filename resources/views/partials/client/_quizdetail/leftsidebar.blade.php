<div class="col-sm-12 col-md-4">
    <div class="sidebar sidebar-left mt-sm-30 ml-40">
        <div class="widget">
            <h4 class="widget-title line-bottom">Courses <span class="text-theme-color-2">List</span></h4>
            <div class="services-list">

                <ul class="list list-border angle-double-right">
                    @foreach($subjects as $subject)
                        <h6><li><a href="{{ route('subject.quizzes.show', ['subject' => $subject]) }}">{{ $subject->name }}</a></li></h6>
                    @endforeach
{{--                    @foreach($jobstests as $jobstest)--}}
{{--                        <li><a href="{{ route }}">{{ $jobstest->name }}</a></li>--}}
{{--                    @endforeach--}}
                </ul>
            </div>
        </div>
    </div>
</div>