<div class="col-md-8 blog-pull-right">
    <div class="single-service">
        <img src="{{ $quiz->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" alt="" class="w-75">
        <h3 class="line-bottom mt-20">{{ $quiz->name }}</h3>

        <h6>{{ $quiz->description }}</h6>

        <form action="{{ route('attempt.index', ['quiz' => $quiz])}}" method="get" class="mt-40">
            @csrf
            <h5>Select Mode</h5>
                <div>
                    <input type="radio" id="mode1" name="mode" value="0" checked>
                    <label for="mode1">Practice mode</label>
                </div>
                <div>
                    <input type="radio" id="mode2" name="mode" value="1">
                    <label  for="mode2">Test mode</label>
                </div>
                <input type="hidden" name="quiz_id" value="{{ $quiz->id }}">
                <button type="submit" class="btn btn-primary customLoginStyle">Start Quiz</button>
        </form>
    </div>
</div>
