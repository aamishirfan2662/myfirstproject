@extends('layouts.client')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="card-body">
                        <form class="form-group" action="{{ route('profile.update', ['profile' => $profile]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('patch')
                            @include('partials.client.profile.form')

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button class="form-control btn btn-primary customLoginStyle" style="background: #F2184E !important;border: 0px !important;" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="container">--}}
{{--        <div class="container">--}}
{{--            <form class="form-group" action="" method="post">--}}
{{--                @csrf--}}
{{--                <div class="container">--}}
{{--                    @include('partials.client.profile.form')--}}
{{--                    <div class="form-group row">--}}
{{--                        <div class="col-md-6">--}}
{{--                            <button class="form-control btn btn-label-primary" type="submit">Save</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection