<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-gray-lightgray text-md-right">{{ __('Avatar') }}</label>
    <div class="col-md-6">
        <img src="{{ Auth::user()->image }}" onerror="this.src='/images/noimage.png'" class="thumbnail border-1px" height="250" width="250" alt="">
    </div>
</div>
<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-gray-lightgray text-md-right">{{ __('Name') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
               value="{{ old('name') ?? $profile->name }}" placeholder="Enter name" maxlength="100" required autocomplete="name" autofocus>

        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" readonly
               value="{{ old('email') ?? $profile->email }}" placeholder="Enter email" maxlength="255" required autocomplete="email">

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="phone" class=" text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

    <div class="col-md-6">
        <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone"
               value="{{ old('phone') ?? $profile->phone }}" placeholder="Enter phone" maxlength="11" autocomplete="phone" autofocus>

        @error('phone')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name" class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('DOB') }}</label>

    <div class="col-md-6">
        <input id="dob" type="date" min="1950-01-01" max="{{ date('Y-m-d') }}" class="form-control @error('dob') is-invalid  @enderror" name="dob"
               value="{{ old('dob') ?? $profile->dob }}" autocomplete="dob" autofocus>

        @error('dob')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name" class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

    <div class="col-md-6">
        <select name="gender" class="form-control">
            <option values="0" {{ $profile->gender == "Female" ? 'selected' : '' }}>Female</option>
            <option values="1" {{ $profile->gender == "Male" ? 'selected' : '' }}>Male</option>
            <option values="2" {{ $profile->gender == "Not Specified" ? 'selected' : '' }}>Not Specified</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('Qualification') }}</label>

    <div class="col-md-6">
        <input id="qualificaion" type="text" class="form-control @error('qualificaion') is-invalid @enderror"
               name="qualificaion" maxlength="255" placeholder="Enter qualification" value="{{ old('qualificaion') ?? $profile->qualification }}" autocomplete="qualificaion" autofocus>

        @error('qualificaion')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('Present Status') }}</label>

    <div class="col-md-6">
        <input type="text" class="form-control @error('present_Status') is-invalid @enderror" name="present_status"
               value="{{ old('present_status') ?? $profile->present_status }}" placeholder="What are you doing now a days" maxlength="255" autocomplete="present_status" autofocus>

        @error('present_Status')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

    <div class="col-md-6">
        <textarea class="form-control @error('address') is-invalid @enderror" name="address" placeholder="Enter address" autocomplete="address" autofocus>{{ old('address') ?? $profile->address }}</textarea>

        @error('address')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>


<div class="form-group row">
    <label class="text-gray-lightgray col-md-4 col-form-label text-md-right">{{ __('Profile Image') }}</label>

    <div class="col-md-6">
        <input type="file" class="form-control @error('image') is-invalid @enderror" name="image"
               value="{{ old('image') }}" autocomplete="image" autofocus>

        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>