<div class="header-top bg-theme-color-2 sm-text-center p-0">
    @if($errors->first('_email') || $errors->first('_password'))
        <div class="alert alert-danger" id="success-alert">
            <strong class="text-white mr-2">Incorrect </strong>credentials.
        </div>
    @endif
    <div class="container customHeaderContainer">
        <div class="row">
            <div class="col-md-12">
                <div class="widget no-border m-0" id="hideResponsive">
                    <ul class="list-inline font-15 sm-text-center mt-10">
                        <li>
                            <a class="text-white" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="text-white">|</li>
                        <li>
                            <a class="text-white" href="{{ route('about') }}">About us</a>
                        </li>
                        <li class="text-white">|</li>
                        <li>
                            <a class="text-white" href="{{ route('contact') }}">Contact us</a>
                        </li>
                        <li class="text-white">|</li>
                        <li>
                            <a class="text-white" href="{{ route('useruploads.create') }}" target="_blank">Add Data</a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCw_9uqq4lCsbCPC6hKJ9mDg/?guided_help_flow=5"><i class="fab font-22 text-white fa-youtube" style="font-size: 40px !important;padding: 0px;margin: -26px;position: absolute;margin-left: -2px;"></i></a>
                        </li>
                        <li class="flex flex-column" style="margin-left: 12%">
                            <span class="welcome">Welcome to Learn Relief</span>
                        </li>


                    @if(\Auth::user())
                        <li class="nav-item dropdown customDropDownUser" onmouseover="openDropDown()" onmouseleave="closeDropDown()">
                            <img alt="" class="inline-block flex-row customImage" height="50" src="{{ Auth::user()->image }}" onerror="this.src='/images/noimage.png'">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
{{--                                <span class="caret"></span>--}}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" >
                                @if(Auth::user()->is_admin)
                                    <a class="dropdown-item" href="{{ route('admin.index') }}">
                                        Admin
                                    </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('profile.show', ['profile' => Auth::user() ]) }}">
                                    Edit profile
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                     @endif
                    </ul>
                </div>
                <div class="widget no-border m-0" id="showResposnive" style="display: none">
                    <img src="{{asset('images/humburgerIcon.png')}}" class="imageBurger" />
                </div>
                <div class="widget no-border m-0">
                    <ul class="openUlBox">
                        <li>
                            <a class="text-white" href="{{ route('home') }}">Home</a>
                        </li>
                        <li>
                            <a class="text-white" href="{{ route('about') }}">About us</a>
                        </li>
                        <li>
                            <a class="text-white" href="{{ route('contact') }}">Contact us</a>
                        </li>
                        <li>
                            <a class="text-white" href="{{ route('useruploads.create') }}">Add Data</a>
                        </li>
                        <li> <a href="https://www.youtube.com/channel/UCw_9uqq4lCsbCPC6hKJ9mDg/?guided_help_flow=5" target="_blank"><i class="fab text-white fa-youtube"style="color: red"></i></a></li>

                        @if(\Auth::user())
                            <li class="nav-item dropdown customDropDownUser2" onmouseover="openDropDown()" onmouseleave="closeDropDown()">
                                <img alt="" class="inline-block flex-row customImage" height="50" src="{{ Auth::user()->image }}" onerror="this.src='/images/noimage.png'">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->is_admin)
                                        <a class="dropdown-item" href="{{ route('admin.index') }}">
                                            Admin
                                        </a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('profile.show', ['profile' => Auth::user() ]) }}">
                                        Edit profile
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
