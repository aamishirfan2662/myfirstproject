<div class="header-middle p-0 bg-lightest xs-text-center customStyleHeader">
    <div class="container customContainer">
        <div class="row">
            <div class="col-md-2 customsetColMd3Logo">
                <div class="widget no-border m-0">
                    <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="{{ route('home') }}"><img src="{{ asset('assets/images/logo.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-md-10 pull-right" id="loginForm">
                <div class="widget no-border sm-text-center mt-10 mb-10 m-0 customClasswidget">
                    @guest
                        <form method="POST" class="customForm" action="{{ route('login') }}">
                            @method('POST')
                            @csrf
                            <div class="form-group row customRowFormGroup">
{{--                                <label for="email" class="col-lg-6">{{ __('E-Mail Address') }}</label>--}}

                                <div class="col-md-12">
                                    <input id="email" placeholder="Email/Phone" class="form-control-sm customClass @error('_email') is-invalid @enderror" maxlength="100" name="_email" value="{{ old('_email') }}" required autofocus>
{{--                                    @error('_email')--}}
{{--                                    <span class="font-9 text-danger" role="alert">--}}
{{--                                        <strong>{{ $errors->first('_email') }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
                                </div>
                            </div>

                            <div class="form-group row customRowFormGroup">
{{--                                <label for="password" class="col-lg-6">{{ __('Password') }}</label>--}}

                                <div class="col-md-12">
                                    <input id="password" type="password" placeholder="Password" class="form-control-sm customClass @error('_password') is-invalid @enderror" maxlength="100" name="_password" value="{{ old('_password') }}" required autocomplete="current-password">

                                    @if (Route::has('password.request'))
                                        <br/>
                                        <span>
                                            <a class="customPasswordReset" href="{{ route('password.request') }}" style="position: inherit; float: left">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        </span>
                                    @endif
{{--                                    @error('_password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
                                </div>
                            </div>

                            <div class="form-group row customRowCheckBox">
                                <div class="col-lg-6">
                                    <div class="form-check customRemember" style="display: none;">

                                        <span class="customTextCheck"><input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}></span>
                                        <span class="customTextTRemember"><label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4 customLoginSection" style="bottom: 15px">
                                    <button type="submit" class="btn btn-primary customLoginStyle" style="background: #F2184E !important;border: 0px !important;">
                                        {{ __('Login') }}
                                    </button>
                                    @if (Route::has('register'))
                                        <a class="btn btn-primary customRegisterStyle" style="background: #F2184E !important;border: 0px !important;" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    @endif
{{--                                    <button type="submit" class="btn btn-primary customRegisterStyle" style="background: #F2184E !important;border: 0px !important;">--}}
{{--                                        {{ __('Register') }}--}}
{{--                                    </button>--}}
                                </div>
                            </div>
                        </form>
                    @else
                        @php
                            $userId = (\Auth::user()->id);
                            $totalQuestions = \App\ConciseRecord::where('user_id', '=', $userId)->sum('total_questions');
                            $correctAnswers = \App\ConciseRecord::where('user_id', '=', $userId)->sum('correct_answers');
                            $avgCorrectAnswers = \App\ConciseRecord::where('user_id', '=', $userId)->avg('correct_answers');
                        @endphp
                        <div class="customContainer mb-1">
                            @if(Auth::user()->getOriginal('image'))
                                @if(file_exists(storage_path('app/public/'.Auth::user()->getOriginal('image'))))
                                    <div class="inline-block align-top">
                                        <a href="{{ route('profile.show', ['profile' => Auth::user() ]) }}"><img src="{{ Auth::user()->image }}" class="border-radius-10px" height="60" width="60" alt="">
                                        </a>
                                    </div>
                                @endif
                            @endif
                            <div class="inline-block align-left">
                                <h6>questions attempted: <span class="setColor">{{ \App\ConciseRecord::where('user_id', Auth::id())->sum('total_questions') }}</span></h6>
                                <h6>correct answers: <span class="setColor">{{ \App\ConciseRecord::where('user_id', Auth::id())->sum('correct_answers') }}</span></h6>
                                <h6>wrong answers: <span class="setColor">{{ \App\ConciseRecord::where('user_id', Auth::id())->sum('total_questions') - \App\ConciseRecord::where('user_id', Auth::id())->sum('correct_answers') }}</span></h6>
                            </div>
                        </div>
{{--                        <div class="customContnaainer flex flex-row border-1px">--}}
{{--                        </div>--}}
{{--                        <div class="container customContainer ">--}}
{{--                            --}}
{{--                        </div>--}}
                    @endguest

                </div>
            </div>

        </div>
    </div>
</div>
