<div class="header-nav">
    <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-colored border-bottom-theme-color-2-1px">
        <div class="container customNavContainer">
            <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
                <ul class="menuzord-menu">
                    @foreach($categories as $category)
                            @if($category->subCategories->count() > 0)
                            <li><a href="#">{{ $category->name }}</a>
                                <ul class="dropdown">
                                    @foreach($category->subCategories as $subCategory)
                                        @if($category->id == 1)
                                        <li><a href="{{ route('jobs.jobs', ['subCategory' => $subCategory]) }}">{{ $subCategory->name }}</a></li>
                                        @else
                                            <li><a href="{{ route('test.quizzes.show', ['subCategory' => $subCategory]) }}">{{ $subCategory->name }}</a>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
{{--                            @else--}}
{{--                                <li><a href="{{ route('test.quizzes.show', ['category' => $category]) }}">{{ $category->name }}</a>--}}
                            @endif
                    @endforeach

                    @if($subjects->count() > 0)
                        <li><a href="#">Subjects</a>
                            <ul class="dropdown" style="max-height: 200px; overflow: scroll; overflow-x: hidden">
                                @foreach($subjects as $subject)
                                    <li><a href="{{ route('subject.quizzes.show', ['subject' => $subject]) }}">{{ $subject->name }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endif

                    <li><a href="{{ route('client.blogs') }}">Blogs</a>

                    <li><a href="{{ route('client.news') }}">News</a>

                    <li>
                        <a href="{{ route('client.scholarships') }}">Scholarships</a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{ route('client.past-papers') }}">Past Papers</a>--}}
{{--                    </li>--}}
                    <li>
                        <a href="{{ route('client.job-ads') }}">Find Jobs</a>
                    </li>

                    @if($downloadables->count() > 0)
                        <li><a href="#">Downloads</a>
                            <ul class="dropdown">
                                @foreach($downloadables as $downloadable)
                                    <li>
                                        <a href="{{ route('client.downloadables.show', ['downloadable' => $downloadable]) }}">{{ $downloadable->category }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endif

                </ul>
            </nav>
        </div>
    </div>
</div>
