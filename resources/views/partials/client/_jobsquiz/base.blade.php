@extends('layouts.client')

@section('content')
  <section class="customContainerPb">
    <div class="">
      <div class="row">
        <div class="col-md-12 blog-pull-right">
          <div class="row">
            @if($job->quizzes->count() > 0)
              @foreach($job->quizzes as $quiz)
                @if($quiz->files->count() > 0)
                  <div class="col-md-3">
                    <div class="service-block bg-light">
                      <div class="thumb"> <img alt="featured project" src="{{ $quiz->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" class="img-fullwidth">
                      </div>
                      <div class="content text-left flip p-25 pt-0">
                        <h4 class="line-bottom mt-2 mb-10">{{ $quiz->name }}</h4>
                        <h6>{{ $quiz->description }}</h6>
                        <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('job.quizzes.detail', ['subCategory' => $job->subCategory, 'job' => $job,'quiz' => $quiz]) }}">view details</a>
                      </div>
                    </div>
                  </div>
                @else
                  <div class="col-md-9 blog-pull-right form-group">
                    <h5 class="alert alert-warning">No quizzes available</h5>
                  </div>
                    @break
                @endif
              @endforeach
            @else
              <div class="col-md-9 blog-pull-right form-group">
                <h5 class="alert alert-warning">No quizzes available</h5>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection