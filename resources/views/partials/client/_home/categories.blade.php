@foreach($categories as $category)
    @foreach($category->subCategories as $subCategory)
        @if($subCategory->jobs->count() > 0)
            <section class="{{ ($subCategory->id % 2 != 0) ? "bg-lighter" : '' }}">
                <div class="customContainerPb pb-60">
                    <div class="section-title mb-10">
                        <div class="row">
                            <div class="col-md-8">
                                <h2 class="mt-0 text-uppercase font-28 line-bottom line-height-1"><span class="text-theme-color-2 font-weight-400">{{ $subCategory->name }}</span></h2>
                            </div>
                        </div>
                    </div>
                        <div class="section-content">
                            <div class="row multi-row-clearfix">
                                <div class="col-md-12">
                                    @foreach($subCategory->jobs->take(4) as $job)
                                    <div class="col-md-3">
                                        <div class="item">
                                            <div class="service-block">
                                                <div class="thumb"> <img alt="featured project" src="{{ $job->image }}" height="230" width="260" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" class="img-fullwidth">
                                                </div>
                                                <div class="content border-1px border-bottom-theme-color-2-2px bg-light p-15 clearfix">
                                                    <h5 class="line-bottom mt-2 mb-8">{{ $job->name }}</h5>
                                                    <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('job.quizzes.show', ['subCategory' => $subCategory, 'job'=>$job]) }}">view courses</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="col-lg-5">

                                </div>
                                <div class="col-lg-2">
                                    <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10 w-100" href="{{ route('jobs.jobs', ['subCategory' => $subCategory]) }}">view more</a>
                                </div>
                                <div class="col-lg-5">

                                </div>
                            </div>
                        </div>

                </div>
            </section>
        @elseif($subCategory->quizzes->count() > 0)
            <section class="{{ ($subCategory->id % 2 != 0) ? "bg-lighter" : '' }}">
                <div class="customContainerPb pb-60">
                    <div class="section-title mb-10">
                        <div class="row">
                            <div class="col-md-8">
                                <h2 class="mt-0 text-uppercase font-28 line-bottom line-height-1"><span class="text-theme-color-2 font-weight-400">{{ $subCategory->name }}</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="section-content">
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($subCategory->quizzes->take(4) as $quiz)
                                    <div class="col-md-3">
                                        <div class="item">
                                            <div class="service-block">
                                                <div class="thumb"> <img alt="featured project" height="230" width="260" src="{{ $quiz->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" class="img-fullwidth">
                                                </div>
                                                <div class="content border-1px border-bottom-theme-color-2-2px bg-light p-15 clearfix">
                                                    <h5 class="line-bottom mt-2 mb-8">{{ $quiz->name }}</h5>
                                                    <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('test.quizzes.detail', ['subCategory' => $quiz->quizable, 'quiz' => $quiz]) }}">view detail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-lg-5">

                            </div>
                            <div class="col-lg-2">
                                <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10 w-100" href="{{ route('test.quizzes.show', ['subCategory' => $subCategory]) }}">view more</a>
                            </div>
                            <div class="col-lg-5">

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach

@endforeach


{{--  </div>--}}
