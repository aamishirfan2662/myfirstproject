@if($news->count() > 0)
    <section id="blog" class="">
        <div class="customContainerPb pb-60">
            <div class="section-title mb-10">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="mt-0 text-uppercase font-28 line-bottom line-height-1">Latest <span class="text-theme-color-2 font-weight-400">News</span></h2>
                    </div>
                </div>
            </div>
            <div class="section-content">
                <div class="row">
                    @foreach($news as $new)
                        <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                            <article class="post clearfix mb-sm-30">
                                <div class="entry-header">
                                    <div class="post-thumb thumb">
                                        <img src="{{ $new->image }}" height="1280" width="1920" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" alt="" class="img-responsive img-fullwidth">
                                    </div>
                                </div>
                                <div class="entry-content p-20 pr-10 bg-light">
                                    <div class="entry-meta media mt-0 no-bg no-border">
                                        <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                            <ul>
                                                <li class="font-16 text-white font-weight-600 border-bottom">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->day}}</li>
                                                <li class="font-12 text-white text-uppercase">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->shortMonthName}}</li>
                                            </ul>
                                        </div>
                                        <div class="media-body pl-15">
                                            <div class="event-content pull-left flip">
                                                <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="{{ route('client.news.show', ['news' => $new]) }}">{{ $new->title }}</a></h4>
                                                {{--                                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>--}}
                                                {{--                                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <h6 class="mt-10">{{ $new->excerpt }}</h6>
                                    <a href="{{ route('client.news.show', ['news' => $new]) }}" class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10">Read more</a>
                                    <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>
                    @endforeach

                </div>
                    <div class="col-lg-5">

                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10 w-100" href="{{ route('client.news') }}">view more</a>
                    </div>
                    <div class="col-lg-5">

                    </div>
            </div>
        </div>
    </section>
@else
@endif
