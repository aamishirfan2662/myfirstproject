@if(file_exists(storage_path('app/public/'.$banner->getOriginal('image'))))

{{--    @if(!strpos(Request::url(), 'start'))--}}
    <a href="{{$banner->hyper_link}}">
{{--    @endif--}}
        <section class="divider" data-background-ratio="12" data-background-image="{{ $banner->image ?? asset('assets/images/logo.jpeg') }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" data-bg-img="{{ $banner->image }}">
            <div class="container pb-150">
            </div>
        </section>
{{--    @if(!strpos(Request::url(), 'start'))--}}
    </a>
{{--    @endif--}}
@endif
