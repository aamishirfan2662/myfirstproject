@if($subjects->count() > 0)
    <section class="bg-lighter">
        <div class="customContainerPb pb-60">
            <div class="section-title mb-10">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="mt-0 text-uppercase font-28 line-bottom line-height-1"><span class="text-theme-color-2 font-weight-400">Subjects</span></h2>
                    </div>
                </div>
            </div>
            <div class="section-content">
                <div class="row multi-row-clearfix">
                    <div class="col-md-12">
                        @foreach($subjects->take(4) as $subject)
                            <div class="col-md-3 sm-text-center">
                                <div class="item ">
                                    <div class="service-block">
                                        <div class="thumb"> <img alt="featured project" height="230" width="260" src="{{ $subject->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" class="img-fullwidth">
                                        </div>
                                        <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                                            <h4 class="line-bottom mt-2 mb-8">{{ $subject->name }}</h4>
                                            <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('subject.quizzes.show', ['subject' => $subject]) }}">view detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
{{--                            <div class="col-lg-5">--}}

{{--                            </div>--}}
{{--                            <div class="col-lg-2">--}}
{{--                                <a c/lass="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10 w-100" href="{{ route('subjects') }}">view more</a>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-5">--}}

{{--                            </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif