@extends('layouts.client')

@section('content')
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0"></script>
  <div class="main-content">

    <!-- Section: Blog -->
    <section>
      <div class="container">
        <div class="row">

          {{-- left-side-bar--}}
          @include('partials.client._newsdetail.leftsidebar')

          {{--blog--}}
          @include('partials.client._newsdetail.blog')

        </div>
      </div>
    </section>
  </div>
@endsection