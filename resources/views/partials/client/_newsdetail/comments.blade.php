<div class="comments-area">
    <h5 class="comments-title mt-2">Comments</h5>
    <ul class="comment-list">

        @if($news->comments->count() > 0)
            @foreach($news->comments as $comment)
                <li>
                    <div class="media comment-author"><img class="mr-10 border-radius-25px" height="25" src="{{ ($comment->user->image) }}" onerror="this.src='{{ asset('images/noimage.png') }}'" alt="">
                        <div class="media-body">
                            <h5 class="media-heading comment-heading">{{ $comment->user->name }}</h5>
                            <div class="comment-date text-theme-color-2">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comment->created_at)}}</div>
                            <h6 class="mt-4">{{ $comment->body }}</h6>
                            @auth
                                <form action="{{ route('replies.store', ['blog' => $news]) }}" method="post">
                                    @csrf
                                    <textarea class="form-control w-100 no-border" name="reply" rows="1"></textarea>
                                    <input type="hidden" name="comment_id" value="{{ $comment->id }}">
                                    <button type="submit" class="replay-icon pull-right text-theme-colored btn-link" href="#"> <i class="fa fa-reply text-theme-colored"></i>Reply</button>
                                </form>
                            @endauth
                            @foreach($comment->replies as $reply)
                                <div class="clearfix"></div>
                                <div class="media comment-author nested-comment"><img class="border-radius-30px" alt="" height="40" src="{{ ($comment->user->image) ?? asset('assets/images/blog/comment2.jpg') }}">
                                    <div class="media-body p-20 bg-lighter">
                                        <h5 class="media-heading comment-heading">{{ $reply->user->name }}</h5>
                                        <div class="comment-date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $reply->created_at)}}</div>
                                        <h6 class="mt-4">{{ $reply->body }}</h6>
                                        @auth
                                            <form action="{{ route('replies.store', ['blog' => $news]) }}" method="post">
                                                @csrf
                                                <textarea class="form-control w-100 no-border" name="reply" rows="1"></textarea>
                                                <input type="hidden" name="comment_id" value="{{ $reply->id }}">
                                                <button type="submit" class="replay-icon pull-right text-theme-colored btn-link" href="#"> <i class="fa fa-reply text-theme-colored"></i> Reply</button>
                                            </form>
                                        @endauth
                                        @foreach($reply->replies as $rep)
                                            <div class="clearfix"></div>
                                            <div class="media comment-author nested-comment"><img class="border-radius-30px" alt="" height="40" src="{{ ($comment->user->image) ?? asset('assets/images/blog/comment2.jpg') }}"></a>
                                                <div class="media-body p-20 bg-lighter">
                                                    <h5 class="media-heading comment-heading">{{ $rep->user->name }}</h5>
                                                    <div class="comment-date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $rep->created_at)}}</div>
                                                    <h6 class="mt-4">{{ $rep->body }}</h6>
                                                    @auth
                                                        <form action="{{ route('replies.store', ['blog' => $news]) }}" method="post">
                                                            @csrf
                                                            <textarea class="form-control w-100 no-border" name="reply" rows="1"></textarea>
                                                            <input type="hidden" name="comment_id" value="{{ $reply->id }}">
                                                            <button type="submit" class="replay-icon pull-right text-theme-colored btn-link"> <i class="fa fa-reply text-theme-colored"></i> Reply</button>
                                                        </form>
                                                    @endauth
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </li>
            @endforeach
        @else
            <h6>no comments on this news</h6>
        @endif
    </ul>
</div>
<div class="comment-box">
    <div class="row">
        @auth
            <div class="col-sm-12">
                <h5>Leave a Comment</h5>
                <div class="row">
                    <form role="form" id="comment-form" action="{{ route('comments.store', ['blog' => $news]) }}" method="post">
                        @csrf
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control w-100 @error('body') is-invalid @enderror" name="body" id="contact_message2"  placeholder="Enter Message" cols="100"></textarea>
                                @error('body')
                                <span class="invalid-feedback danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="hidden" name="blog" value="{{ $news->id }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-dark btn-flat pull-right m-0" data-loading-text="Please wait...">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @else
            <div>
                <span><h5 class="alert-outline-info colored-rounded" >Please register/login to comment on this news.</h5></span>
            </div>
        @endauth
    </div>
{{--</div>style="color: #0f3e68"--}}