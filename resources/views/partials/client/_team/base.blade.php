{{--@extends('layouts.client')--}}

{{--@section('content')--}}
    <section>
        <div class="container">
            <div class="section-title mb-10">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="mt-0 text-uppercase font-28 line-bottom line-height-1">Our <span class="text-theme-color-2 font-weight-400">Teachers</span></h2>
                    </div>
                </div>
            </div>
            <div class="section-content">
                <div class="row multi-row-clearfix">
                    <div class="col-sm-6 col-md-3 sm-text-center mb-sm-30">
                        <div class="team maxwidth400">
                            <div class="thumb"><img class="img-fullwidth" src="assets/images/team/team5.jpg" alt=""></div>
                            <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                                <h4 class="name text-theme-color-2 mt-0">Andre Smith - <small class="text-white">Teacher</small></h4>
                                <p class="mb-20">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-left flip">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                                <a class="btn btn-theme-colored btn-sm pull-right flip" href="page-teachers-details.html">view details</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 sm-text-center mb-sm-30">
                        <div class="team maxwidth400">
                            <div class="thumb"><img class="img-fullwidth" src="assets/images/team/team6.jpg" alt=""></div>
                            <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                                <h4 class="name mt-0 text-theme-color-2">Sakib Smith - <small class="text-white">Teacher</small></h4>
                                <p class="mb-20">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-left flip">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                                <a class="btn btn-theme-colored btn-sm pull-right flip" href="page-teachers-details.html">view details</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 sm-text-center mb-sm-30">
                        <div class="team maxwidth400">
                            <div class="thumb"><img class="img-fullwidth" src="assets/images/team/team7.jpg" alt=""></div>
                            <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                                <h4 class="name mt-0 text-theme-color-2">David Zakaria - <small class="text-white">Teacher</small></h4>
                                <p class="mb-20">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-left flip">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                                <a class="btn btn-theme-colored btn-sm pull-right flip" href="page-teachers-details.html">view details</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 sm-text-center">
                        <div class="team maxwidth400">
                            <div class="thumb"><img class="img-fullwidth" src="assets/images/team/team8.jpg" alt=""></div>
                            <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                                <h4 class="name mt-0 text-theme-color-2">Ismail Jon - <small class="text-white">Teacher</small></h4>
                                <p class="mb-20">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-left flip">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                                <a class="btn btn-theme-colored btn-sm pull-right flip" href="page-teachers-details.html">view details</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{{--@endsection--}}