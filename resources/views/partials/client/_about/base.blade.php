@extends('layouts.client')

@section('content')
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: About -->
    @include('partials.client._about.about')

    <!-- Section: Services -->
{{--    @include('partials.client._about.services')--}}


{{--  <!-- Divider: Funfact -->--}}
{{--    @include('partials.client._about.funfacts')--}}


{{--  <!-- Section: Why Choose Us -->--}}
{{--    @include('partials.client._about.whychooseus')--}}


  </div>
  <!-- end main-content -->
@endsection