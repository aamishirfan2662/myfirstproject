st<section class="">
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="letter-space-4 text-uppercase mt-0 mb-0">All About</h6>
                    <h2 class="text-uppercase font-weight-600 mt-0 font-28 line-bottom">{{ $about->primary_text }}</h2>
                </div>
                <div class="col-md-9">
                    <h4>{{ $about->secondary_text }}</h4>
                </div>
            </div>
        </div>
    </div>
</section>