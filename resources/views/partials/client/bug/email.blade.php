@component('mail::message')

There is a bug <strong>{{ $data['bug'] }}</strong> in quiz <strong>{{ $data['quiz'] }}</strong><br> at question <strong>{{ $data['question'] }}</strong>.

@component('mail::button', ['url' => url()->previous()])
view bug
@endcomponent
{{ config('app.name') }}
@endcomponent
