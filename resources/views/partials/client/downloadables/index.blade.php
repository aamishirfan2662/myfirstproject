@extends('layouts.client')

@section('content')
    <section class="bg-lighter">
        <div class="container">
            <h4 class="mb-5">{{ $category }}</h4>
            <div class="row">
                <div class="col-md-12 blog-pull-right">
                    <div class="row">
                        @if($files->count() > 0)
                            @foreach($files as $file)
                                <div class="col-md-3">
                                    <div class="service-block bg-light">
                                        <div class="content text-left flip p-25 pt-0">
                                            <h4 class="line-bottom mt-2 mb-10">{{ $file->name }}</h4>
                                            <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{ route('client.downloadable.download', ['downloadable' => $file->id]) }}">Download</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-9 blog-pull-right form-group">
                                <h5 class="alert alert-warning">No downloadables available</h5>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
{{--        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid mt-20">--}}
{{--            <div class="kt-portlet kt-portlet--mobile">--}}
{{--                @if($files)--}}
{{--                <div class="kt-portlet__body kt-portlet__body--fit">--}}
{{--                    <!--begin: Datatable -->--}}
{{--                    <table width="100%" class="table table-striped table-dark table-hover table-bordered">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th title="Field #2">File Name</th>--}}
{{--                            <th title="Field #3">Actions</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach($files as $file)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $file->name }}</td>--}}
{{--                                --}}{{--                                <td><a href="{{ route('jobsTests.show', $jobTest->id) }}">{{ $jobTest->name }}</a></td>--}}
{{--                                <td>--}}
{{--                                    <span><a href="{{ route('client.downloadable.download', ['downloadable' => $file]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Download</a></span>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}

{{--                    <!--end: Datatable -->--}}
{{--                </div>--}}
{{--                @else--}}
{{--                    <div class="container">--}}
{{--                        <div class="col-md-9 blog-pull-right form-group">--}}
{{--                            <h5 class="alert alert-warning">No downloadables available</h5>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}
@endsection