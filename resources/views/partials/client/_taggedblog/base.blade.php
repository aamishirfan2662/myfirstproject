@extends('layouts.client')

@section('content')
  <div class="main-content">

    <!-- Section: News & Blog -->
    <section id="news">
      <div class="customContainerPb">

        {{--blogs--}}
        @include('partials.client._taggedblog.blogs')

      </div>
    </section>
  </div>
@endsection