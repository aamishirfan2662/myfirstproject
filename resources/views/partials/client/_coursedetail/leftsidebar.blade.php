<div class="col-sm-12 col-md-4">
    <div class="sidebar sidebar-left mt-sm-30 ml-40">
        <div class="widget">
            <h4 class="widget-title line-bottom">Other <span class="text-theme-color-2">Categories</span></h4>
            <div class="services-list">

                <ul class="list list-border angle-double-right">
                    @foreach($categories as $category)
                        @if($category->subCategories->count() > 0)
                            @foreach($category->subCategories as $subCategory)
                                @if($category->id == 1)
                                    <h6><li><a href="{{ route('jobs.jobs', ['subCategory' => $subCategory]) }}">{{ $subCategory->name }}</a></li></h6>
                                @else
                                    <h6><li><a href="{{ route('test.quizzes.show', ['subCategory' => $subCategory]) }}">{{ $subCategory->name }}</a></li></h6>
                                @endif
                            @endforeach
                        @else
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>