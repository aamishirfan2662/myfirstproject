@extends('layouts.admin')

@section('content')

    @include('partials.admin.fileupload.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">File ID</th>
                        <th title="Field #2">File Name</th>
                        <th>Subject</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                            <tr>
                                <td scope="row">{{ $file->id }}</td>
                                <td>{{ $file->name }}</td>
                                <td>{{ $file->fileable->name }}</td>
                                <td>
                                    <form action="{{ route('files.destroy', ['file' => $file->id]) }}" method="post">
                                        <span><a href="{{ route('files.show', ['subject' => $file->fileable, 'file' => $file,]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="deleteFile(event, {{$file->quizzes}})" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10 deleteFile">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
    <script>


        function deleteFile(event, quizzes) {

            if (quizzes.length)
            {
                let c = confirm("This file has associations with one or many quizzes. Are you sure your want to delete this file. It will also be deleted from corresponding quizzes")
                if (!c){
                    event.preventDefault();
                }

            }
            else {
                let c = confirm("Are you sure you want to delete this file? It will be deleted permanently")

                if(!c){
                    event.preventDefault();
                }
            }
        }
    </script>
@endsection