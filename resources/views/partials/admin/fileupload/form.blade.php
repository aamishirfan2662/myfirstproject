<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Dashboard</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">File</span><span><a href="{{ asset('sample.xlsx') }}">Sample File</a></span>
        </div>
    </div>
</div>
<div class="kt-portlet__body">

    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter file name" maxlength="100" value="{{ old('name') ?? $file->name }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="subjects">Subjects</label>
        <select class="form-control" name="fileable_id">
            @foreach($subjects as $subject)
                <option value="{{ $subject->id }}"{{ $subject->id == $file->fileable_id ? 'selected' : '' }}>{{ $subject->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="image">File</label>
        <input type="file" class="form-control @error('file') is-invalid @enderror" name="file">
        @error('file')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
