
<div class="kt-portlet__body">
    <div class="form-group form-group-last">
        <div class="alert alert-secondary" role="alert">
            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
            <div class="alert-text">
                Jobs/Tests Info.
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter file name" maxlength="100" value="{{ old('name') ?? $jobs_test->name }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="subjects">Category</label>
        <select class="form-control" name="sub_category_id">
            @foreach($subCategories as $subCategory)
                <option value="{{ $subCategory->id }}"{{ $subCategory->id == $jobs_test->sub_category_id ? 'selected' : '' }}>{{ $subCategory->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Image</label>
        <input type="file" class="form-control @error('image') is-invalid @enderror" name="image">
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>

