@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('jobs-tests.update', ['jobs_test' => $jobs_test]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @include('partials.admin.--jobtest.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            @if($jobs_test->image)
                <div class="align-center ml-500">
                    <img src="{{ asset('storage/'.$jobs_test->image) }}" class="img-fullwidth" alt="">
                </div>
            @endif
        </form>
        <!--end::Form-->
    </div>
@endsection