@extends('layouts.admin')

@section('content')

    @include('partials.admin.--jobtest.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Job/Test ID</th>
                        <th title="Field #2">Job/Test Name</th>
                        <th title="Field #3">Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($jobsTests as $jobsTest)
                            <tr>
                                <td scope="row">{{ $jobsTest->id }}</td>
                                <td>{{ $jobsTest->name }}</td>
{{--                                <td><a href="{{ route('jobsTests.show', $jobTest->id) }}">{{ $jobTest->name }}</a></td>--}}
                                <td>{{ $jobsTest->subCategory->name }}</td>
                                <td>
                                    <form action="{{ route('jobs-tests.destroy', ['jobs_test' => $jobsTest]) }}" method="post" class="inline-block">
                                        <span><a href="{{ route('jobs-tests.show', $jobsTest) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection