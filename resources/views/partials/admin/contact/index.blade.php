@extends('layouts.admin')

@section('content')

    <div class="kt-portlet">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">Contact</span>
                </div>
            </div>
        </div>

                <form class="kt-form" action="{{ route('contact.update', ['contact' => $contact]) }}" style="padding: 20px" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone"  class="form-control @error('phone') is-invalid @enderror" maxlength="100" placeholder="Enter phone" value="{{ old('phone') ?? $contact->phone  }}">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address"  class="form-control @error('address') is-invalid @enderror" placeholder="Enter address">{{ old('address') ?? $contact->address }}</textarea>
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"  class="form-control @error('email') is-invalid @enderror" placeholder="Enter email" value="{{ old('email') ?? $contact->email }}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-10">
                                    <button type="submit" class="btn btn-brand btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

    </div>


@endsection