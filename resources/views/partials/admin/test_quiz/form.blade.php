<div class="kt-portlet__body" id="kt-portlet" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="form-group form-group-last">
        <div class="alert alert-secondary" role="alert">
            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
            <div class="alert-text">
                Test's Quiz
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
               placeholder="Enter file name" maxlength="100" value="{{ old('name') ?? $quiz->name }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    {{--    <input type="text" v-model="firstName" class="form-control">--}}

    @if($quiz->id)
        <div>
            <table width="100%" class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th title="Field #1">File is</th>
                    <th title="Field #2">File Name</th>
                    <th title="Field #3">Subject Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($quiz->files as $file)
                    <tr>
                        <td scope="row">{{ $file->id }}</td>
                        <td>{{ $file->name }}</td>
                        <td>{{ $file->fileable->name }}</td>
                        <td>
{{--                            <a href="{{ route('course.destroyFile', ['quiz'=> $quiz , 'file_id' => $file->id]) }}" v-on:click="deleteFile($event)" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</a>--}}
                            <a href="Javascript:;" data-quiz="{{$quiz}}" data-id="{{$file->id}}" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10 deleteFileJs">Delete</a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
    @endif
    <div v-for="(incr, index) in increment">
        <div class="form-group">
            <label>Subjects</label>
            <select class="form-control @error('subject') is-invalid @enderror" name="subject[]" id="subject"
                    @change="getVal($event, index)">
                <option value="" disabled selected>Choose subject</option>
                @foreach($subjects as $subject)
                    @if($subject->files->count() > 0)
                        <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                    @else
                    @endif
                @endforeach
            </select>
            @error('subject')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>


        <div class="form-group">
            <label>Files</label>
{{--            <span v-for="t in total_files">--}}
{{--                @{{ t }}--}}
{{--            </span>--}}
            <select class="form-control @error('file') is-invalid @enderror" name="file[]" multiple>
                <option v-for="file in total_files[index]" :value="file.id">@{{ file.name }}</option>
            </select>
            @error('file')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="kt-portlet__foot">
            <div class="row">
                <div class="col-10">
                    <button
                            class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10"
                            id="inc"
                            name="inc"
                            type="button"
                            v-on:click="increments">
                        <b>+</b>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="2" name="quizable_id">

    <div class="form-group">
        <label>Description</label>
        <textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description"
                  placeholder="Enter description">{{ old('description') ?? $quiz->description }}</textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="subjects">Category</label>
        <select class="form-control" name="quizable_id">
            @foreach($subCategories as $subCategory)
                <option value="{{ $subCategory->id }}"{{ $subCategory->id == $quiz->quizable_id ? 'selected' : '' }}>{{ $subCategory->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Image</label>
        <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" value="{{ old('image') ?? $quiz->image }}">
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>

<script>
    $(function () {

        $('body').on('click', '.deleteFileJs', function() {
            let c = confirm('are you sure you want to delete this file?')
            if (c){
                let quiz = ($(this).data('quiz'));
                let id = ($(this).data('id'));
                route = `/admin/test-quizz/${quiz['id']}/${id}`;
                window.location.href = (route);
            }
        });

        var app = new Vue({
            el: '#kt-portlet',

            mounted() {

                // this.getJobs(document.getElementById('category').value);
            },

            data: {
                selectedFiles: [],
                subject: '',
                category: '',
                increment: 1,
                files: [],
                filess: [],
                total_files: [{
                    'name': 'kitty'
                }],
                tf: [],
                file_name: '',
                jobs: [],
            },

            watch: {
                subject: function (val) {
                    // console.log(val);
                    this.subjectFiles(this.subject);
                    // // console.log(this.files[0]);
                },

                // category: function (val) {
                //     // console.log(this.category);
                //     this.getJobsTests(this.category);
                // },
                total_files:{
                    handler(newVal) {
                        console.log('property updated', newVal);
                    },
                    deep: true
                },
            },

            methods: {

                getJobs: function($event)
                {
                    this.getJob($event.target.value);
                },
                xgetVal: function (e) {
                    if (e.target.options.selectedIndex > -1)
                    {
                        let arr = []
                        let data = e.target.options[e.target.options.selectedIndex].value
                        this.subjectFiles(data)
                        this.selectedFiles.push(this.subjectFiles(data))

                        arr = this.selectedFiles.filter(value => value !== data)

                //         console.log(arr)
                    }
                //     // console.log(e.target.value);
                    // this.subjectFiles(e.target.value);
                },
                getVal: function (e, index) {
                    let sub = e.target.value;
                    // console.log("e.target.value: "+e.target.value);
                    // console.log("index: "+index);
                    this.subjectFiles(sub, index)
                },
                subjectFiles: function (subject, index) {
                    let that  = this;
                    axios.get(`/api/files/${subject}`)
                        .then(response => {
                            this.total_files[index] = (response.data);
                            console.log(this.total_files);
                            let t = this.total_files;
                            this.total_files = [...t];
                        })
                    // console.log('this.total_files');
                    // console.log(this.total_files);
                    // console.log(Object.keys(this.total_files).length);
                    // this.total_files_count = Object.keys(this.total_files).length;
                    // console.log('this.total_files');
                },

                getJob: function (category) {
                    axios.get(`/api/jobs/${category}`)
                        .then(response => {
                            // console.log('response');
                            // console.log(response.data);
                            this.jobs = response.data;
                            this.jobs = this.jobs.data
                        })
                },

                increments: function ($event) {
                    this.increment += 1;
                    // console.log(this.increment);
                    // console.log(this.filess);
                },


            },
        })
    })
</script>
