@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('test-quizzes.store') }}" method="post" enctype="multipart/form-data">
            @csrf

{{--            ahsan--}}
{{--            @{{ message }}--}}
{{--            <input type="text" v-model="firstName" class="form-control">--}}
            @include('partials.admin.test_quiz.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-brand btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

@endsection