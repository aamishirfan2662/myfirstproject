@extends('layouts.admin')

@section('content')

    @include('partials.admin.test_quiz.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Quiz ID</th>
                        <th title="Field #2">Quiz Name</th>
                        <th title="Field #2">Category Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($quizzes as $quiz)
                            <tr>
                                <td scope="row">{{ $quiz->id }}</td>
                                <td>{{ $quiz->name }}</td>
                                <td>{{ $quiz->quizable->name }}</td>
                                <td>
                                    <form action="{{ route('test-quizzes.destroy', ['test_quiz' => $quiz->id]) }}" method="post">
                                        <span><a href="{{ route('test-quizzes.show', ['category' => $quiz->quizable, 'test_quiz' => $quiz]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row" style="font-size: 12px">
                    <div class="col-12 d-flex justify-content-center pt-3">
                        {{$quizzes->links()}}
                    </div>
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection