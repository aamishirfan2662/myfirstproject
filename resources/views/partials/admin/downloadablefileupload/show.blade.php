@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('downloadables.update', ['downloadable' => $file->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @include('partials.admin.downloadablefileupload.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>
@endsection