@extends('layouts.admin')

@section('content')

    @include('partials.admin.downloadablefileupload.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">File ID</th>
                        <th title="Field #2">File Name</th>
                        <th title="Field #2">Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                            <tr>
{{--                                {{ dd($file->fileable) }}--}}
                                <td scope="row">{{ $file->id }}</td>
                                <td>{{ $file->name }}</td>
                                <td>{{ $file->fileable->category }}</td>
                                <td>
                                    <form action="{{ route('downloadables.destroy', ['downloadable' => $file->id]) }}" method="post">
                                        <span><a href="{{ route('downloadables.show', ['category' => $file->fileable, 'downloadable' => $file]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection