@extends('layouts.admin')

@section('content')

    <div class="kt-portlet">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">Slider</span>
                </div>
            </div>
        </div>
                @foreach($sliders as $slider)

                <form class="kt-form" action="{{ route('slider.update', ['slider' => $slider]) }}" style="padding: 20px" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="image">Slide {{ $slider->id }} Image:</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') ?? $slider->image }}">
                                @error('image')
                                <span class="invalid-feedback" role="alert">
{{--                                    <strong>{{ $message }}</strong>--}}
                                </span>
                                @enderror
                            </div>
                            <img src="{{ $slider->image }}"  height="100px" width="150px" class="float-right">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Slide {{ $slider->id }} Primary Text</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('primary_text') is-invalid @enderror" name="primary_text[]" id="pwd" placeholder="Enter primary test" maxlength="20" value="{{ old('primary_text') ?? $slider->primary_text }}">
                                @error('primary_text')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Slide {{ $slider->id }} Secondary Text</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('secondary_text') is-invalid @enderror" id="pwd" name="secondary_text[]" placeholder="Enter secondary test" maxlength="30" value="{{ old('secondary_text') ?? $slider->secondary_text }}">
                                @error('secondary_text')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-brand btn btn-primary">Submit</button>
                            </div>
                        </div>

                </form>
            @endforeach

                <!--end: Datatable -->
    </div>

@endsection