@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('tests.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('partials.admin.---test.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-brand btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>
@endsection