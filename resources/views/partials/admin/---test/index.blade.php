@extends('layouts.admin')

@section('content')

    @include('partials.admin.---test.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Test ID</th>
                        <th title="Field #2">Test Name</th>
                        <th title="Field #3">Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tests as $test)
                            <tr>
                                <td scope="row">{{ $test->id }}</td>
                                <td>{{ $test->name }}</td>
                                <td>{{ $test->category->name }}</td>
                                <td>
                                    <form action="{{ route('tests.destroy', ['test' => $test]) }}" method="post" class="inline-block">
                                        <span><a href="{{ route('tests.show', $test) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection