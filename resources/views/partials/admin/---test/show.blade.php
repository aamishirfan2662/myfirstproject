@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('tests.update', ['test' => $test]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @include('partials.admin.---test.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            @if($test->image)
                <div class="align-center ml-500">
                    <img src="{{ asset('storage/'.$test->image) }}" class="img-fullwidth" alt="">
                </div>
            @endif
        </form>
        <!--end::Form-->
    </div>
@endsection