
<div class="kt-portlet__body">
    <div class="form-group form-group-last">
        <div class="alert alert-secondary" role="alert">
            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
            <div class="alert-text">
                Tests Info.
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter test name" maxlength="100" value="{{ old('name') ?? $test->name }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label>Image</label>
        <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') ?? $test->image }}">
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>

