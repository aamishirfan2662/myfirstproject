@extends('layouts.admin')


@section('content')
    @include('partials.admin.userupload.contentheader')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Upload ID</th>
                        <th title="Field #2">Uploader Name</th>
                        <th title="Field #3">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($useruploads as $userupload)
                            <tr>
                                <td scope="row">{{ $userupload->id }}</td>
                                <td>{{ $userupload->name }}</td>
                                <td>
                                    <form action="{{ route('useruploads.destroy', ['userupload' => $userupload]) }}" method="post" class="inline-block">
                                        <span><a href="{{ route('downloads.download', ['userupload' => $userupload]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Download</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection