@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('subjects.update', ['subject' => $subject]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @include('partials.admin.subject.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>

                </div>
            </div>
{{--            {{ dd($subject->image) }}--}}
            @if($subject->image)
                <div>
                    <div class="align-center p-5">
                        <img src="{{ $subject->image }}" height="250" width="250" alt="">
                    </div>
                </div>
            @endif
        </form>
        <!--end::Form-->
    </div>
@endsection