@extends('layouts.admin')

@section('content')

    <div class="kt-portlet">

        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">About</span>
                </div>
            </div>
        </div>
                <form class="kt-form" action="{{ route('about.update', ['about' => $about]) }}" style="padding: 20px" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Primary Text</label>
                            <input type="text" name="primary_text"  class="form-control @error('primary_text') is-invalid @enderror" maxlength="100" placeholder="Enter primary text" value="{{ old('primary_text') ?? $about->primary_text }}">
                            @error('primary_text')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Secondary Text</label>
                            <textarea type="text" name="secondary_text"  class="form-control @error('secondary_text') is-invalid @enderror" placeholder="Enter secondary text">{{ old('secondary_text') ?? $about->secondary_text }}</textarea>
                            @error('secondary_text')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-10">
                                    <button type="submit" class="btn btn-brand btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

    </div>


@endsection