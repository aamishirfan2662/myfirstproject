<div class="kt-portlet__body" id="kt-portlet">
    <div class="form-group form-group-last">
        <div class="alert alert-secondary" role="alert">
            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
            <div class="alert-text">
                Subject's Quiz.
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter file name" maxlength="100" value="{{ old('name') ?? $quiz->name }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    @if($quiz->id)
        <div>
            <table width="100%" class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th title="Field #1">File is</th>
                    <th title="Field #2">File Name</th>
                    <th title="Field #3">Subject Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($quiz->files as $file)
                    <tr>
                        <td scope="row">{{ $file->id }}</td>
                        <td>{{ $file->name }}</td>
                        <td>{{ $file->fileable->name }}</td>
                        <td>
                            <a href="Javascript:;" data-quiz="{{$quiz}}" data-id="{{$file->id}}" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10 deleteFileJs">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
    @endif
    <div class="form-group">
        <label>Subjects</label>
        <select class="form-control @error('subject') is-invalid @enderror" name="quizable_id" id="subject" v-model="subject">
            <option value="" disabled selected>Choose subject</option>
            @foreach($subjects as $subject)
                @if($subject->files->count() > 0)
                    <option value="{{ $subject->id }}" {{ $subject->id == $quiz->quizable_id ? 'selected' : '' }}>{{ $subject->name }}</option>
                @else
                @endif
            @endforeach
        </select>
        @error('subject')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    <div class="form-group">
        <label>Files</label>
        <select class="form-control @error('file') is-invalid @enderror" name="file[]" multiple>
            <option v-for="file in files" :key="file.id" :value="file.id">@{{ file.name }}</option>
        </select>
        @error('file')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label>Description</label>
        <textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Enter description">{{ old('description') ?? $quiz->description }}</textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label>Image</label>
        <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" value="{{ old('image') ?? $quiz->image }}" >
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

{{--    <Subjects :subjects = "{{ $subjects }}"></Subjects>--}}
{{--    <div class="form-group">--}}
{{--        <label for="image">File</label>--}}
{{--        <input type="file" class="form-control @error('file') is-invalid @enderror" name="file">--}}
{{--        @error('file')--}}
{{--        <span class="invalid-feedback" role="alert">--}}
{{--            <strong>{{ $message }}</strong>--}}
{{--        </span>--}}
{{--        @enderror--}}
{{--    </div>--}}
</div>


<script>
    $(function () {

        $('body').on('click', '.deleteFileJs', function() {
            let c = confirm('are you sure you want to delete this file?')
            if (c){
                let quiz = ($(this).data('quiz'));
                let id = ($(this).data('id'));
                route = `/admin/quiz/${quiz['id']}/${id}`;
                window.location.href = (route);
            }
        });

        var app = new Vue({
            el: '#kt-portlet',

            // mounted(){
            //     // var subject = document.getElementById('subject').value;
            //     // this.subjectFiles(subject);
            //     // console.log('mounted working fine');
            //     // console.log(subject);
            // },

            data: {
                subject: '',
                files: [],
                file_name:''
            },

            watch: {
                subject: function (val) {
                    console.log(val);
                    this.subjectFiles(this.subject);
                    // console.log(this.files[0]);
                },
            },

            methods: {
                subjectFiles: function (subject) {
                    // alert($event.target.value);
                    // let subject = $event.target.value;
                    axios.get(`/api/files/${subject}`)
                        .then(response => {
                            console.log('response');
                            console.log(response.data);
                            this.files = response.data;
                        })
                },
            },
        })
    })
</script>