@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('subject-quizzes.update', ['subject_quiz' => $quiz]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @include('partials.admin.subject_quiz.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

@endsection