@extends('layouts.admin')

@section('content')

    <div class="kt-portlet">

        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">Banner</span>
                </div>
            </div>
        </div>
                <form class="kt-form" action="{{ route('banner.update', ['banner' => $banner]) }}" style="padding: 20px" method="post" enctype="multipart/form-data">
                    @csrf

{{--                        {{ asset('storage/sliders/'.$slider->image) }}--}}
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="image">Banner Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control @error('image') is-invalid @enderror" name="image"  value="{{ old('image') ?? $banner->image }}">
                            </div>
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="hyper_link">Hyper link</label>
                            <div class="col-sm-10">
                                <input type="url" class="form-control @error('hyper_link') is-invalid @enderror" name="hyper_link"  value="{{ old('hyper_link') ?? $banner->hyper_link }}">
                            </div>
                            @error('hyper_link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-brand btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <img src="{{ $banner->image }}" height="200px">
                        </div>

                </form>

                <!--end: Datatable -->
    </div>

@endsection