@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('tags.update', ['tag' => $tag]) }}" method="post">
            @csrf
            @method('PATCH')
            @include('partials.admin.tag.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
@endsection