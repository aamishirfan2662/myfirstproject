<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Dashboard</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">Blog</span>
        </div>
    </div>
</div>
<div class="kt-portlet__body">
    <div class="form-group">
        <label>Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Enter title" maxlength="100" value="{{ old('title') ?? $blog->title }}">
        @error('title')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label>Excerpt</label>
        <input type="text" class="form-control @error('excerpt') is-invalid @enderror" name="excerpt" placeholder="Enter excerpt" value="{{ old('excerpt') ?? $blog->excerpt }}" maxlength="100">
        @error('excerpt')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label>body</label>
        <textarea id="body" name="body" class="@error('body') is-invalid @enderror">{{ old('body') ?? $blog->body }}</textarea>
        @error('body')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <div><label>Tags</label></div>
        <select name="tags[]" class="form-control" multiple>
            @foreach($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <div><label>Type</label></div>
        <select name="type" class="form-control">
            <option value="blog" {{ $blog->type == 'blog' ? 'selected' : '' }}>Blog</option>
            <option value="news" {{ $blog->type == 'news' ? 'selected' : '' }}>News</option>
            <option value="scholarships" {{ $blog->type == 'scholarships' ? 'selected' : '' }}>Scholarships</option>
            <option value="job's ads" {{ $blog->type == 'job_ads' ? 'selected' : '' }}>Job's Ads</option>
        </select>
    </div>
    <div class="form-group">
        <label>Title Image</label>
        <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') ?? $blog->image }}">
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>
