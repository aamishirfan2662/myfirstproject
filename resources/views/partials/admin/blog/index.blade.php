@extends('layouts.admin')

@section('content')

    @include('partials.admin.blog.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Blog ID</th>
                        <th title="Field #2">Blog Title</th>
                        <th title="Field #2">Type</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($blogs as $blog)
                            <tr>
                                <td scope="row">{{ $blog->id }}</td>
                                <td>{{ $blog->title }}</td>
                                <td>{{ $blog->type }}</td>
{{--                                <td><a href="{{ route('jobsTests.show', $jobTest->id) }}">{{ $jobTest->name }}</a></td>--}}
                                <td>
                                    <form action="{{ route('blogs.destroy', ['blog' => $blog]) }}" method="post" class="inline-block">
                                        <span><a href="{{ route('blogs.show', ['blog' => $blog] ) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row" style="font-size: 12px">
                    <div class="col-12 d-flex justify-content-center pt-3">
                        {{$blogs->links()}}
                    </div>
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection