@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('jobs.update', ['job' => $job->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @include('partials.admin.job.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            @if($job->image)
                <div>
                    <div class="align-center p-5">
{{--                        {{dd($job->image)}}--}}
                        <img src="{{ $job->image }}" height="250" width="250" class="img-fullwidth" alt="">
                    </div>
                </div>
            @endif
        </form>
        <!--end::Form-->
    </div>
@endsection