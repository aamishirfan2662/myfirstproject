@extends('layouts.admin')

@section('content')

    @include('partials.admin.job.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Job ID</th>
                        <th title="Field #2">Job Name</th>
                        <th title="Field #3">Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($jobs as $job)
                            <tr>
                                <td scope="row">{{ $job->id }}</td>
                                <td>{{ $job->name }}</td>
{{--                                <td><a href="{{ route('jobsTests.show', $jobTest->id) }}">{{ $jobTest->name }}</a></td>--}}
                                <td>{{ $job->subCategory->name }}</td>
                                <td>
                                    <form action="{{ route('jobs.destroy', ['job' => $job->id]) }}" method="post">
                                        <span><a href="{{ route('jobs.show', ['subCategory' => $job->subCategory, 'job' => $job]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="destroyQuizzes(event, {{$job->quizzes}})" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10 destroyQuizzes">Delete</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="row" style="font-size: 12px">
                    <div class="col-12 d-flex justify-content-center pt-3">
                        {{$jobs->links()}}
                    </div>
                </div>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

    <script>


        function destroyQuizzes(event, quizzes) {

            if (quizzes.length)
            {
                let c = confirm("This job has associations with one or many quizzes. Are you sure your want to delete this Job. It will also delete the corresponding quizzes")
                if (!c){
                    event.preventDefault();
                }

            }
            else {
                let c = confirm("Are you sure you want to delete this Job?")

                if(!c){
                    event.preventDefault();
                }
            }
        }
    </script>

@endsection