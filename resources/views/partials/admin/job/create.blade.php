@extends('layouts.admin')

@section('content')
    <div class="kt-portlet">

        <!--begin::Form-->
        <form class="kt-form" action="{{ route('jobs.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('partials.admin.job.form')
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-10">
                            <button type="submit" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>
@endsection