<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Dashboard</h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <span class="kt-subheader__desc">Jobs</span>
        </div>
    </div>
</div>
<div class="kt-portlet__body">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter file name" maxlength="100" value="{{ old('name') ?? $job->name }}">
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="subjects">Category</label>
        <select class="form-control" name="sub_category_id">
            @foreach($subCategories as $subCategory)
                <option value="{{ $subCategory->id }}"{{ $subCategory->id == $job->sub_category_id ? 'selected' : '' }}>{{ $subCategory->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Image</label>
        <input type="file" class="form-control @error('image') is-invalid @enderror" value="{{ old('image') ?? $subCategory->image }}" name="image">
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>

