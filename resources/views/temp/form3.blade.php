<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Input States
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form">
        <div class="kt-portlet__body">
            <div class="form-group form-group-last">
                <div class="alert alert-secondary" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                    <div class="alert-text">
                        Add the disabled or readonly boolean attribute on an input to prevent user interactions.
                        Disabled inputs appear lighter and add a <code>not-allowed</code> cursor.
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Disabled Input</label>
                <input type="email" class="form-control" disabled="disabled" placeholder="Disabled input">
            </div>
            <div class="form-group">
                <label>Disabled select</label>
                <select class="form-control" disabled="disabled">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleTextarea">Disabled textarea</label>
                <textarea class="form-control" disabled="disabled" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label>Readonly Input</label>
                <input type="email" class="form-control" readonly placeholder="Readonly input">
            </div>
            <div class="form-group">
                <label for="exampleTextarea">Readonly textarea</label>
                <textarea class="form-control" readonly rows="3"></textarea>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="reset" class="btn btn-brand">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </form>

    <!--end::Form-->
</div>