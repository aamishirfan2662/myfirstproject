<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Learn Relief | Education & Courses online" />
    <meta name="keywords" content="academy, course, education, elearning, learning," />
    <meta name="author" content="ThemeMascot" />

    <!-- Page Title -->
    <title>Learn Relief | Education & Courses online</title>

    <!-- Favicon and Touch Icons -->
    <link href="{{ asset('assets/images/favicon.png') }}" rel="shortcut icon" type="image/png">
    <link href="{{ asset('assets/images/apple-touch-icon.png') }}" rel="apple-touch-icon">
    <link href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}" rel="apple-touch-icon" sizes="72x72">
    <link href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}" rel="apple-touch-icon" sizes="114x114">
    <link href="{{ asset('assets/images/apple-touch-icon-144x144.png') }}" rel="apple-touch-icon" sizes="144x144">
    <link href="{{asset('assets/css/styles.css')}}" rel="stylesheet" type="text/css">
    <!-- Stylesheet -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/css-plugin-collections.css') }}" rel="stylesheet"/>
    <!-- CSS | menuzord megamenu skins -->
    <link id="menuzord-menu-skins" href="{{ asset('assets/css/menuzord-skins/menuzord-rounded-boxed.css') }}" rel="stylesheet"/>
    <!-- CSS | Main style file -->
    <link href="{{ asset('assets/css/style-main.css') }}" rel="stylesheet" type="text/css">
    <!-- CSS | Preloader Styles -->
    <link href="{{ asset('assets/css/preloader.css') }}" rel="stylesheet" type="text/css">
    <!-- CSS | Custom Margin Padding Collection -->
    <link href="{{ asset('assets/css/custom-bootstrap-margin-padding.css') }}" rel="stylesheet" type="text/css">
    <!-- CSS | For Dark Layout -->
    <link href="{{ asset('assets/css/style-main-dark.css') }}" rel="stylesheet" type="text/css">
    <!-- CSS | Responsive media queries -->
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
    <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

    <!-- Revolution Slider 5.x CSS settings -->
    <link  href="{{ asset('assets/js/revolution-slider/css/settings.css') }}" rel="stylesheet" type="text/css"/>
    <link  href="{{ asset('assets/js/revolution-slider/css/layers.css') }}" rel="stylesheet" type="text/css"/>
    <link  href="{{ asset('assets/js/revolution-slider/css/navigation.css') }}" rel="stylesheet" type="text/css"/>

    <!-- CSS | Theme Color -->
    <link href="{{ asset('assets/css/colors/theme-skin-color-set-1.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- external javascripts -->
    <script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <!-- JS | jquery plugin collection for this theme -->
    <script src="{{ asset('assets/js/jquery-plugin-collection.js') }}"></script>

    <!-- Revolution Slider 5.x SCRIPTS -->
    <script src="{{ asset('assets/js/revolution-slider/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>

        .thumb > img {
            height: 340px;
            object-fit: cover;
        }
        .customClass{
            width: 100%;
            border: 1px solid #F0F0F0;
        }
        .customLoginStyle{
            background: #F2184E !important;
            border: 0px !important;
            padding: 5px 12px 5px 12px;
        }
        .customRegisterStyle{
            position: relative;
            left: 10px;
            padding: 5px 12px 5px 12px;
        }
        .customStyleHeader{
        }
        .customClasswidget{
            /*float: left !important;*/
            width: 85%;
            text-align: right;
        }
        .customForm{
            display: flex;
            margin-bottom: 0px;
            margin-top: 8px;
        }
        .customRowFormGroup{
            margin-right: 10px;
        }
        .customRowCheckBox{
            display: flex;
            align-items: center;
        }
        .customRemember{
            display: flex;
            width: 145px;
            align-items: center;
        }
        .customStyleLogin{
            position: absolute;
            right: 10px;
            bottom: 10px;
            border: 1px solid #C0C0C0 !important;
            border-radius: 2px;
        }
        .customTextTRemember{
            position: relative;
            left: 22px;
            top: 10px;
        }
        .customLoginSection{
            display: flex;
            align-items: center;
            margin-left: 0px;
            position: relative;
            bottom: 7px;
        }
        .customContainer{
            max-width: 100% !important;
            min-width: 100% !important;
        }
        .customsetColMd3Logo{
            display: flex;
            padding-left: 24px;
        }
        #loginForm{
            display: flex;
            align-items: center;
        }
        .customPasswordReset{
            position: absolute;
            bottom: 10px;
            left: 365px;
            font-size: 0.875rem;
        }
        .customDropDownUser{
            float: right;
            color: #fff;
            position: relative;
            bottom: 2px;
            display: flex !important;
        }
        .customDropDownUser2{
            float: left;
            color: #fff;
            position: relative;
            bottom: 2px;
            display: flex !important;
        }
        #navbarDropdown{
            color: #fff !important;
        }
        .nav-link.dropdown-toggle:after, .btn.dropdown-toggle:after{
            opacity: 10 !important;
            margin-left: 0px !important;
        }
        .tp-leftarrow{
            display: none !important;
        }
        .tp-rightarrow{
            display: none !important;
        }
        html, body{
            height: auto !important;
        }
        .customImage{
            border-radius: 26px;
            height: 25px;
            width: 25px;
            object-fit: cover;
            position: relative;
            top: 5px;
        }
        .customContainer p{
            margin-bottom: 0px;
            font-size: 14px;
            font-weight: 500;
            text-transform: uppercase;
        }
        .setColor{
            color:red;
            font-weight:600;
        }
        /*.thumb img{*/
        /*    height: 140px;*/
        /*    object-fit: cover;*/
        /*}*/
        section > .container, section > .container-fluid {
            /*padding-top: 40px !important;*/
            padding-bottom: 70px;
        }
        /*.PSC{*/
        /*    background: #fff !important;*/
        /*}*/
        /*.Admission{*/
        /*    background: #fff !important;*/
        /*}*/
        .customHeaderContainer{
            max-width: 98%;
        }
        #loginForm {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            padding-right: 30px;
        }
        .customNavContainer{
            max-width: 100%;
        }
        .customContainer{
            max-width: 95%;
            /*padding: 5%;*/
        }
        .customContainerPb{
            max-width: 100%;
            padding: 5%;
        }
        .customBackGroundCH{
            background-color: #F2184E;
        }
        .colorWhite{
            color: white;
        }
        .customGrey{
            color: #b3b3b3;
        }
        #showResposnive{
            display: none !important;
        }
        @media (max-width: 991px) {
            #showResposnive{
                display: block !important;
                height: 50px;
            }
            .welcome{
                margin-left: 130px;
            }
            .imageBurger{
                float: right;
                position: relative;
                height: 16px;
                top: 14px;
            }
            #hideResponsive{
                display: none;
            }
            .customContainer p {
                margin-bottom: 0px;
                font-size: 14px;
                font-weight: 500;
                text-transform: uppercase;
                text-align: end;
            }
            .customForm {
                display: block;
                margin-bottom: 0px;
                margin-top: 8px;
            }
            .customsetColMd3Logo {
                display: flex;
                padding-left: 24px;
                justify-content: center;
            }
            .customPasswordReset {
                position: unset;
                font-size: 0.875rem;
            }
            .customLoginSection{
                display: block;
            }
            .customRowFormGroup {
                 margin-right: 0px;
            }
            .openUlBox li {
                text-align:left;
                font-size: 15px;
                width: 100%;
                margin-top: 12px;
                margin-bottom: 12px;
            }
        }

        @media (max-width: 650px) {
            .welcome{
                margin-left: 5px;
            }
            .customForm {
               display: block;
                margin-bottom: 0px;
                margin-top: 8px;
            }
            .customsetColMd3Logo {
                display: flex;
                padding-left: 24px;
                justify-content: center !important;
            }

        }

    </style>

    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5e70a97f67e62a0012871b7f&product=inline-share-buttons" async="async"></script>
</head>
<body class="dark">
<div id="wrapper" class="clearfix">

    <!-- Header -->
    @include('partials.client._header.base')

    <!-- Start main-content -->

{{--    @include('partials.client._team.base')--}}
    @yield('content')

    <!-- Footer -->
    @include('partials.client._footer.base')
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{ asset('assets/js/custom.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
      (Load Extensions only on Local File Systems !
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/revolution-slider/js/extensions/revolution.extension.video.min.js') }}"></script>
<script>
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
    // function openDropDown() {
    //     $('.dropdown-menu ').addClass('show');
    // }
    // function closeDropDown() {
    //     $('.dropdown-menu ').removeClass('show');
    // }
    // $('.openUlBox').hide();
    // $('.imageBurger').click(function () {
    //     $('.openUlBox').slideToggle();
    // });
</script>
<script>
    $("#upload-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#upload-alert").slideUp(500);
    });
    function openDropDown() {
        $('.dropdown-menu ').addClass('show');
    }
    function closeDropDown() {
        $('.dropdown-menu ').removeClass('show');
    }
    $('.openUlBox').hide();
    $('.imageBurger').click(function () {
        $('.openUlBox').slideToggle();
    });
</script>
<script src="https://code.responsivevoice.org/responsivevoice.js?key=Xx2NvCsB"></script>
</body>
</html>
