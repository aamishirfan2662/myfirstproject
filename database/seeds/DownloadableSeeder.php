<?php

use App\Downloadable;
use Illuminate\Database\Seeder;

class DownloadableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Downloadable::create([
            'category' => 'Newspapers & Magazines'
        ]);

        Downloadable::create([
            'category' => "Books"
        ]);

        Downloadable::create([
            'category' => 'MCQs'
        ]);

        Downloadable::create([
            'category' => 'Institutes & Courses'
        ]);
    }
}
