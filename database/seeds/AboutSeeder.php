<?php

use App\About;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        About::create([
            'primary_text' => 'The Best E-Learning platform all over Pakistan',
            'secondary_text' => 'The Best E-Learning platform all over Pakistan. Having dozens of different quizzes for the preparation of any sort of Job or Test.',
        ]);
    }
}
