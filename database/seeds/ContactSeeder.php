<?php

use App\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'phone' => 'XXX-XXXX-XXX',
            'address' => 'not specified',
            'email' => 'not specified',
        ]);
    }
}
