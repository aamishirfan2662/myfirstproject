<?php

use App\Category;
use App\JobTest;
use App\SubCategory;
use App\Subject;
use App\Tag;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            UserSeeder::class,
            SubCategorySeeder::class,
            SliderSeeder::class,
            AboutSeeder::class,
            ContactSeeder::class,
            DownloadableSeeder::class,
            BannerSeeder::class
        ]);

//        JobTest::create([
//            'name' => 'Clerk',
//            'sub_category_id'  => '1'
//        ]);
//
//        JobTest::create([
//            'name' => 'ECAT',
//            'sub_category_id'  => '2'
//        ]);
//
//        Subject::create([
//            'name' => 'Biology',
//        ]);
//
//        Tag::create([
//            'name' => 'laravel',
//        ]);
    }
}
