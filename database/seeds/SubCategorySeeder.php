<?php

use App\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        SubCategory::create([
//            'name' => 'Departmental',
//            'category_id' => '1',
//            'image' => 'sub_categories/departmental.png'
//        ]);

        SubCategory::create([
            'name' => 'Federal Public Service Commission',
            'category_id' => '1',
            'image' => 'sub_categories/fpsc.jpg'
        ]);

        SubCategory::create([
            'name' => 'Punjab Public Service Commission',
            'category_id' => '1',
            'image' => 'sub_categories/ppsc.jpeg'
        ]);

        SubCategory::create([
            'name' => 'All Other Public Service Commission',
            'category_id' => '1',
            'image' => 'sub_categories/ppsc.jpeg'
        ]);

        SubCategory::create([
            'name' => 'All Testing Service',
            'category_id' => '1',
            'image' => 'sub_categories/nts.jpeg'
        ]);

        SubCategory::create([
            'name' => 'All Other jobs',
            'category_id' => '1',
            'image' => 'sub_categories/nts.jpeg'
        ]);

        SubCategory::create([
            'name' => 'MCAT',
            'category_id' => '2',
            'image' => 'sub_categories/mcat.png'
        ]);


        SubCategory::create([
            'name' => 'ECAT',
            'category_id' => '2',
            'image' => 'sub_categories/ecat.png'
        ]);

        SubCategory::create([
            'name' => 'HAT',
            'category_id' => '2',
            'image' => 'sub_categories/HAT.png'
        ]);

        SubCategory::create([
            'name' => 'Other Tests',
            'category_id' => '2',
            'image' => 'sub_categories/ecat.png'
        ]);
    }
}
