<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Digital Waves',
            'email' => 'admin@digitalwaves.net',
            'password' => bcrypt('password'),
            'is_admin' => '1',
        ]);

        //dummy user seeder
        User::create([
            'name' => 'Dummy User',
            'email' => 'dummyUser@gmail.com',
            'password' => bcrypt('123456789'),
            'is_admin' => '0',
        ]);
    }
}
